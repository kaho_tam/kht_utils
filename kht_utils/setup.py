import setuptools, datetime, os
from pathlib import Path

print(Path.cwd())
filelist = [ f for f in (Path("./") / 'dist').rglob("*") if (str(f).endswith(".whl") or str(f).endswith(".gz")) ]
for f in filelist:
    os.remove(f)

with open("README.md", "r") as fh:
    long_description = fh.read()
    
setuptools.setup(
    name="kht_utils",
    version='{:%Y%m%d%H%M}'.format(datetime.datetime.now()),
    author="Ka Ho Tam",
    author_email="kaho.tam.ph@gmail.com",
    description="Ka Ho's Util Functions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://kaho_tam@bitbucket.org/kaho_tam/kht_utils.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
