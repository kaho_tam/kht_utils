# KHT's Utilities

## building the wheel file:

# As described: https://packaging.python.org/tutorials/packaging-projects/

# python -m pip install --user --upgrade setuptools wheel

# To do install as a copy, uncomment the lines below:
#rm -r build
#python setup.py sdist bdist_wheel
#pip install ./dist/*.whl
#pip install --ignore-installed ./dist/kht_utils-*-py3-none-any.whl


# Editable Install
pip install -e .
