# KHT's Utilities

## debug

Functions for debugging

## file_io

Functions for handling file input / output

## numeric

Numeric functions

## plot

Plotting functions

## time

Timing utilities

## vartype

Handling variables, parsers, and user input
