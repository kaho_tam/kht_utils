import numpy as np
import argparse
import subprocess
import signal
import warnings
import re
import pathlib
from pathlib import Path
from kht_utils.time import datetimestring
from kht_utils.file_io import json_io

def safe_index(arr, *args, root=True):
    """
    Safe indexing of array
    Usage: safe_index(arr, (i, j, k, ...))
    returns arr[np.minimum(arr.shape[0], np.maximum(0, i)),np.minimum(arr.shape[0], np.maximum(0, j)),np.minimum(arr.shape[0], np.maximum(0, k))]
    Indexes an array without crashing it when index exceeds its size.
    :param arr: array to be sliced
    :param args:
    :param root:
    :return:
    """

    args = args[0]
    sub_arr = arr[np.minimum(arr.shape[0]-1, np.maximum(0, args[0]))]

    if sub_arr.ndim == 1:
        if sub_arr.size == 1:
            return sub_arr
        elif len(args) > 1:
            return sub_arr[np.minimum(sub_arr.shape[0] - 1, np.maximum(0, args[1]))]
        else:
            return sub_arr
    else:
        return safe_index(sub_arr, args[1:], root=False)

def dict_generator(indict, pre=None):
    """
    # Flattens a dict and returns a generator
    # https://stackoverflow.com/questions/12507206/python-how-to-completely-traverse-a-complex-dictionary-of-unknown-depth?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    :param indict: input, dictionary
    :param pre:
    :return:
    """
    pre = pre[:] if pre else []
    if isinstance(indict, dict):
        for key, value in indict.items():
            if isinstance(value, dict):
                for d in dict_generator(value, pre + [key]):
                    yield d
            elif isinstance(value, list) or isinstance(value, tuple):
                for v in value:
                    for d in dict_generator(v, pre + [key]):
                        yield d
            else:
                yield pre + [key, value]
    else:
        yield indict

def getFromDict(dataDict, mapList):
    """
    Get a given data from a dictionary with position provided as a list
    :param dataDict:
    :param mapList:
    :return: dict

    Example:
        >>> dataDict = {'a': {'r': 1, 's': 2, 't': 3}, 'b': {'u': 1, 'v': {'w': 4, 'x': 1, 'y': 2, 'z': 3}, 'w': 3}}
        >>> getFromDict(dataDict, ["b", "v", "w"])
        >>> 4
        >>> setInDict(dataDict, ["b", "v", "w"], 1)
        >>> getFromDict(dataDict, ["b", "v", "w"])
        >>> 1

    """
    for k in mapList: dataDict = dataDict[k]
    return dataDict

def setInDict(dataDict, mapList, value):
    """
    Set a given data in a dictionary with position provided as a list
    :param dataDict:
    :param mapList:
    :param value:

    Example:
        >>> dataDict = {'a': {'r': 1, 's': 2, 't': 3}, 'b': {'u': 1, 'v': {'w': 4, 'x': 1, 'y': 2, 'z': 3}, 'w': 3}}
        >>> getFromDict(dataDict, ["b", "v", "w"])
        >>> 4
        >>> setInDict(dataDict, ["b", "v", "w"], 1)
        >>> getFromDict(dataDict, ["b", "v", "w"])
        >>> 1

    """
    for k in mapList[:-1]: dataDict = dataDict[k]
    dataDict[mapList[-1]] = value

def invert_dict(dictionary):
    """
    Inverts the key and value of a dictionary. Obviously this would only work if key:values are mapped 1:1
    :param dictionary:
    :return: inverted dictionary
    """
    return dict([[v, k] for k, v in dictionary.items()])

def str2bool(v):
    """
    For argparse. to process string to boolean.
    From https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse
    :param v: input
    :return: argument casted to  boolean type
    """
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def universal_parser(parser=None, description="Arguments"):
    """
    Universal Parser that can take in arbitary arguments and converts it into a dict.
    eg. python test.py -x 1 -y thisisastring -z [1,2,a]   ---> returns {'x':1, 'y':'thisisastring', 'z':'[a,1,2]'}
    :param parser:      It is possible to extend from an existing parser.
    :param description: Write something useful
    :return: dictionary containing argument names and values.
    
    Example:
    
    test = anyparser("Put in your arguments")
    print("arguments are: {}".format(test))
    """

    if parser is None:
        parser = argparse.ArgumentParser(description=description)
    # parser.add_argument('-x', required=False, default=None,)
    parsed, unknown = parser.parse_known_args()  # this is an 'internal' method
    # which returns 'parsed', the same as what parse_args() would return
    # and 'unknown', the remainder of that
    # the difference to parse_args() is that it does not exit when it finds redundant arguments

    for arg in unknown:
        if arg.startswith(("-", "--")) and not re.fullmatch(r'[-|\d|\.]+', arg):
            # you can pass any arguments to add_argument
            parser.add_argument(arg, type=str)

    args = parser.parse_args()

    for i, key in enumerate(args.__dict__):
        if (isinstance(args.__dict__[key], str)):
            try:
                args.__dict__[key] = int(args.__dict__[key])
            except:
                try:
                    args.__dict__[key] = float(args.__dict__[key])
                except:
                    pass

    return args
  
def stdinWait(text, time, default=None, timeoutDisplay = None, **kwargs):
    """
    Input with timeout https://stackoverflow.com/questions/1335507/keyboard-input-with-timeout-in-python
    :param text: text to prompt the user for input. Leave a {} in the string if you want to print the time.
    :param time:	time to wait in seconds
    :param default: Default string to return if timeout
    :param timeoutDisplay: String to display if timeout
    :param kwargs:
    :return: input if (not timeout) else default

    Example:
        >>> inp = stdinWait("You have {} seconds enter an input and press <Enter>.", 3, None, "\tInput Timeout.")
        >>> print(inp)
    """

    def interrupt(signum, frame):
        raise Exception("")

    signal.signal(signal.SIGALRM, interrupt)
    signal.alarm(time) # sets timeout
    try:
        inp = input(text.format(time))
        signal.alarm(0)
        timeout = False
    except (KeyboardInterrupt):
        printInterrupt = kwargs.get("printInterrupt", True)
        if printInterrupt:
            print("Keyboard interrupt")
        timeout = True # Do this so you don't mistakenly get input when there is none
        inp = default
    except:
        timeout = True
        if not timeoutDisplay is None:
            print(timeoutDisplay)
        signal.alarm(0)
        inp = default
    return inp
    
def git_hash(short=True):
    '''
    Returns hash of current git commit version in current directory. Useful for logging.
    :param short: returns short hash
    :return: hash
    '''
    if short:
        return subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    else:
        return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])
    

class Options(argparse.Namespace):

    def __init__(self, ParentConfig=None, **kwargs):
        self.datetime = datetimestring()[2:]
        self.rootdir = Path.cwd()
        
        if ParentConfig is None and len(kwargs) == 0:
            ParentConfig = universal_parser()

        if ParentConfig is not None:
            for key in ParentConfig.__dict__:
                setattr(self, key, getattr(ParentConfig, key)) 

        for key in kwargs:
            try:
                setattr(self, key, kwargs[key])
            except Exception as e:
                pass

    def __str__(self):
        """Print options

        It will print both current options and default values(if different).
        It will save options into a text file / [checkpoints_dir] / opt.txt
        """
        message = ''
        message += '----------------- Options ---------------\n'
        for k in sorted([x for x in self.__dir__() if not x.startswith("_")]):
            try:
                if not callable(getattr(self, k)):  # Only print options that can be converted into a str
                    message += '{:>25}: {:<30}\n'.format(str(k), str(getattr(self, k)))
            except Exception:
                pass
        message += '----------------- End -------------------'
        return message

    def default(self, attr, default=None, eval_val=None, required=False):
        """
        Sets the default value to attribute to default if it doesn't exist in self;
        if attr exists in self then set self.attr to eval_val(self.attr)
        :param attr:
        :param default:
        :param eval_val:    A function such that self.attr = eval_val(self.attr)
        :param raises an error if required and not param is not given in parser.
        :return:
        """

        if required and (attr not in self):
            print(f"{attr} is required but not given in parser.")
            raise NameError

        if attr not in self:
            setattr(self, attr, default)

        if eval_val is not None:
            setattr(self, attr, eval_val(getattr(self, attr)))

    def get_opts(self):
        # Parses options into a dict
        opts_dict = {}
        for k in sorted([x for x in self.__dir__() if not x.startswith("_")]):
            if not callable(getattr(self, k)):   # Do not print methods
                opts_dict[k] = getattr(self, k)
                if isinstance(opts_dict[k], pathlib.PosixPath):
                    opts_dict[k] = str(opts_dict[k])

        return opts_dict

    def save_opts(self, path):
        opt_dict = self.get_opts()
        json_io(path, opt_dict)

    def load_options(self, opt_dict, overwrite=True, exclude=[]):
        """
        Load option saved as a json file
        :param opt_dict: This could either be a dict or the path to the json file
        :param overwrite: Whether to overwrite existing keys
        :param exclude: A list containing keys to be excluded from being overwritten
        :return:
        """

        if not isinstance(opt_dict, dict):
            opt_dict = json_io(opt_dict)

        for key in opt_dict:
            try:
                if ((key not in self) or (getattr(self, key) is None) or overwrite) and (key not in exclude):
                    setattr(self, key, opt_dict[key])
            except Exception as e:
                print("{}: {}".format(key, e))
                
        return self

    def parse(self, overwrite_nondefault=False, update_time=False):
        """
        Parse some of the options and then save it
        :param overwrite_nondefault: whether to overwrite options that had already been modified from defaults.
        :param update_time: updates the time
        :return:
        """

        if update_time: self.datetime = datetimestring()[2:]

# ------------- Deprecated function names ---------------

def anyparser(parser=None, description="Arguments"):
    warnings.warn("The function anyparser() has been renamed to universal_parser()", DeprecationWarning)
    return universal_parser(parser=parser, description=description)
