import sys, time, importlib
from PIL import Image
import pathlib
from pathlib import Path
import numbers
import pyvips
try:
    import seaborn as sns
except:
    pass
try:
    import pandas as pd
except:
    pass
try:
    import torch
except:
    pass
import numpy as np
try:
    import matplotlib
    matplotlib.use('Qt5Agg', warn=False)
except:
    pass
import matplotlib.pyplot as plt
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from matplotlib.ticker import Locator
try:
    from visdom import Visdom
except Exception as e:
    pass
try:
    import plotly
    import plotly.graph_objects as go
except:
    pass
try:
    from . import debug
except:
    try:
        import debug
    except:
        pass
try:
    from . import image
except:
    try:
        import image
    except:
        pass
try:
    from . import numeric
except:
    try:
        import numeric
    except:
        pass
try:
    from . import file_io
except:
    import file_io


def multiplot(*args, nrows=0, ncols=0, title=None, figsize=(15,10), savefig=None, opts=None,
              show_axes=False, dpi=100, block=True, return_gcf=False, size_inches=None, mosaic=1,
              mosiac_min_perimeter=True):
    """
    Plots multiple subplot. Attempts to save to path savefig if it is not None
    opts should be a list with length==len(args) that will be executed by exec() at every plot.
    Plots torch tensor automatically.
    If it's a 3D torch.Tensor, axis==0 is colour channel. Swap it to axis==2.
    If it's a 4D torch.Tensor, axis==1 is colour channel. axis==0 is batch. Plot only the 1st batch.
    
    :param args: arrays / tensors / images to be plotted
    :param nrows: # of rows
    :param ncols: # of columns
    :param title: A list of strings - title for each subplot
    :param figsize: 
    :param savefig: path to save figure
    :param opts: string to be interpreted as command after each subplot.
    :param show_axes: 
    :param dpi: 
    :param block: block main process
    :param size_inches: size of figure in inches
    :param mosaic: whether to mosaic the input before plotting - 0: no mosaic;
                                                                 1: mosaic if all inputs are same size
                                                                 2: mosaic all inputs
    :param mosiac_min_perimeter: size of figure in inches
    :param return_gcf: 
    :return: 
    """

    if mosaic and nrows==0 and ncols==0:
        args = list(args)
        for i, arg in enumerate(args):
            if isinstance(arg, pyvips.Image):
                args[i] = image.pyvips2arr(arg)
            elif isinstance(arg, torch.Tensor):
                arg = arg.squeeze()
                if (len(arg.shape) > 2) and (arg.shape[-1] > 4):
                    assert len(arg.shape) < 4
                    arg = arg.permute(1, 2, 0)
                args[i] = np.array(arg)
        if (mosaic > 1) or all((arg.shape==args[0].shape) for arg in args[1:]):
            args = [numeric.normalize(arg) for arg in args]
            if nrows!=0 or ncols!=0:
                mosiac_min_perimeter = False
            args = [image.mosaic(*args, nrows=nrows, ncols=ncols, min_perimeter=mosiac_min_perimeter)]
            nrows=0
            ncols=0

    def convert_format(arg):
        try:
            if isinstance(arg, torch.Tensor):
                if len(arg.shape)==3:
                    if arg.shape[0]!=3:
                        arg = arg[0,:,:]
                    else:
                        arg = torch.squeeze(arg.permute(1, 2, 0))

                elif len(arg.shape)==4:
                    if arg.shape[1]!=3:
                        arg = arg[0,0,:,:]
                    else:
                        arg = torch.squeeze(arg[0,:,:,:]).permute(1,2,0)
                arg = arg.cpu().numpy()
            elif isinstance(arg, pyvips.Image):
                arg = image.pyvips2arr(arg).squeeze()
        except:
            pass

        return arg

    if len(args)==1:
        if type(args[0])==tuple: # If it is a tuple, treat each item as an argument
            args = args[0]
            nplots = len(args)
        else:
            nplots = 1
    else:
        nplots = len(args)

    # Want a number of subplots depending on nplot: 1, 1x2, 2x2, 2x2, 2x3, 2x3, 3x3, 3x3, 3x3, 3x4, 3x4, 3x4, 4x4 ...
    if nrows==0 and ncols==0:
        nrows = np.int(np.ceil(0.5*(np.sqrt(1+4*nplots)-1)))
        ncols = np.int(np.ceil(np.sqrt(nplots)))
    elif nrows==0:
        nrows = np.int(np.ceil(nplots / ncols))
    elif ncols==0:
        ncols = np.int(np.ceil(nplots / nrows))

    if savefig is None:
        plt.ioff()

    if nplots == 1:
        plt.figure(figsize=figsize)
        _arg = convert_format(args[0])
        if _arg.dtype == np.uint16 and _arg.max() > 255:
            _arg = _arg >> 8
        im = plt.imshow(_arg)
        if opts is not None: exec(opts[0])
        if title is not None: plt.title(title)
        if not show_axes: plt.axis('off')
        f = im.get_figure()
    elif np.minimum(nrows, ncols) == 1:
        f, axarr = plt.subplots(nrows, ncols, figsize=figsize)
        for i in range(nplots):
            _arg = convert_format(args[i])
            if _arg.dtype == np.uint16 and _arg.max() > 255:
                _arg = _arg >> 8
            im = axarr[i].imshow(_arg)
            if not show_axes: axarr[i].axis('off')
            if opts is not None: exec(opts[i])
            if title is not None:
                if title[i] is not None: axarr[i].set_title(title[i])
    else:
        f, axarr = plt.subplots(nrows, ncols, figsize=figsize)
        n = 0
        for i in range(nrows):
            for j in range(ncols):
                if n < nplots:
                    _arg = convert_format(args[n])
                    if _arg.dtype == np.uint16 and _arg.max() > 255:
                        _arg = _arg >> 8
                    im = axarr[i,j].imshow(_arg)
                    if not show_axes: axarr[i,j].axis('off')
                    if opts is not None: exec(opts[n])
                    if title is not None:
                        if title[n] is not None: axarr[i,j].set_title(title[n])
                    n = n + 1

    if size_inches is not None:
        f.set_size_inches(*size_inches)

    if (savefig is not None):
        try:
            plt.savefig(savefig, bbox_inches='tight', dpi=dpi)
            plt.close()
            return 0
        except Exception as e:
            print('Cannot save figure.', e)

    plt.show(block=block)

    if return_gcf:
        return plt.gcf()


def tocmap(img, norm=True, cmap='viridis', dtype="uint8"):
    """
    Converts greyscale image to cmap
    :param img: greyscale image
    :param norm: whether to normalise the image to 0-1
    :param cmap:    name of cmap:
                    https://matplotlib.org/2.0.2/examples/color/colormaps_reference.html
                    eg. viridis, plasma, inferno, magma
    :param dtype: dtype
    """
    
    if norm:
        img = numeric.normalize(img)
    
    cm = plt.get_cmap(cmap)
    coloured_image = cm(img)[: ,:, :3]
    
    if dtype == "uint8":
        coloured_image = (255*coloured_image).astype(np.uint8)
    
    return coloured_image


def scatterthumb(x, y, thumb, zoom=None, block=False, opts=None):
    '''
    Scatter Plot with Thumbnail when Mouse Hovers above datapoint
    '''

    fig = plt.figure()
    ax = fig.add_subplot(111)
    line, = ax.plot(x, y, ls="", marker="o")

    # create the annotations box
    im = OffsetImage(thumb[0, :, :], zoom=zoom)
    xybox = (50., 50.)
    ab = AnnotationBbox(im, (0, 0), xybox=xybox, xycoords='data',
                        boxcoords="offset points", pad=0.3, arrowprops=dict(arrowstyle="->"))
    # add it to the axes and make it invisible
    ax.add_artist(ab)
    ab.set_visible(False)

    def hover(event):
        # if the mouse is over the scatter points
        if line.contains(event)[0]:
            # find out the index within the array from the event
            ind, = line.contains(event)[1]["ind"]
            # get the figure size
            w, h = fig.get_size_inches() * fig.dpi
            ws = (event.x > w / 2.) * -1 + (event.x <= w / 2.)
            hs = (event.y > h / 2.) * -1 + (event.y <= h / 2.)
            # if event occurs in the top or right quadrant of the figure,
            # change the annotation box position relative to mouse.
            ab.xybox = (xybox[0] * ws, xybox[1] * hs)
            # make annotation box visible
            ab.set_visible(True)
            # place it at the position of the hovered scatter point
            ab.xy = (x[ind], y[ind])
            # set the image corresponding to that point
            im.set_data(thumb[ind, :, :])
        else:
            # if the mouse is not over a scatter point
            ab.set_visible(False)
        fig.canvas.draw_idle()

    # add callback for mouse moves
    fig.canvas.mpl_connect('motion_notify_event', hover)
    
    if opts is not None:
        exec(opts)
        
    plt.show(block=block)

# Todo: Plots multiple line plots in Visdom
# def vizlines(*args):


#Plots multiple plots in Visdom
def vizplot(*args, **kwargs):

    viz = Visdom(env=kwargs['env'] if 'env' in kwargs else "main")
    startup_sec = 1
    while not viz.check_connection() and startup_sec > 0:
        time.sleep(0.1)
        startup_sec -= 0.1

    nplots = len(args)
    norm = kwargs['norm'] if 'norm' in kwargs else True

    for arg in args:

        if isinstance(arg, Image.Image):
            arg = np.array(arg)

        if arg.ndim == 3:
            if arg.shape[2] == 3:
                arg = np.moveaxis(arg, (0, 1, 2), (1, 2, 0))

        if arg.dtype in ['uint16', 'uint32', 'uint64', 'int16', 'int32', 'int64']:
            arg = arg.astype(float)

        if norm and np.max(arg) != np.min(arg):
            arg = (arg-np.min(arg))/(np.max(arg) - np.min(arg))

        viz.image(arg)


def plotlyline(input, filename="output.html", layout=None, legend=None, mode='markers', auto_open=False, multi_ax=False,
               line_colors=None, **kwargs):
    '''
    Plots and saves data to a plotly html file.

    :param input: One list: A list of numbers
                 Two lists:  (x, y)
    :param filename:
    :param layout: Either "plotly go.Layout object" or a dict {"title":"title, "xlabel":"foo", "ylabel":"bar"}
    :param legend:
    :param mode: 'line', 'markers', 'lines+markers'
    :param auto_open:
    :param multi_ax: multiple y axes?
    :param line_colors: A list of colors - eg. ['rgb(255, 0, 0)', 'rgb(0, 255, 0)']
    :return:
    '''

    data = []

    if isinstance(input[0], numbers.Number):
        # One list:
        data.append([np.linspace(0, len(input), len(input)+1), input])
    elif isinstance(input[0][0], numbers.Number):
        for y in input:
            data.append([np.linspace(0, len(y), len(y) + 1), y])
    elif len(input[0])==len(input[1]):
        for x, y in zip(input[0], input[1]):
            data.append([x, y])
    else:
        for y in input[1]:
            data.append([input[0][0], y])

    if legend is None:
        legend = ["trace {}".format(i) for i, _ in enumerate(data)] # [None]*len(data)

    traces = []
    for i, _data in enumerate(data):
        traces.append(go.Scatter(
            x = _data[0],
            y = _data[1],
            mode = mode if isinstance(mode, str) else mode[i],
            name = legend[i],
            yaxis = 'y{}'.format(i+1) if (len(data) > 1 and multi_ax and i > 0) else 'y',
            line_color = line_colors[i] if ((line_colors is not None) and len(line_colors) > i) else None,
            **kwargs,
        ))

    if isinstance(layout, dict):
        layout = dict(title=layout['title'] if 'title' in layout else None,
                      xaxis = dict(title = layout['xlabel']) if 'xlabel' in layout else None,
                      yaxis = dict(title = layout['ylabel']) if 'ylabel' in layout else None)

    if len(data) > 1 and multi_ax:
        if layout is None:
            layout = {}
        layout['xaxis'] = dict(domain=[0.1*np.int((len(data)-1)/2), 1.0-0.1*np.int((len(data)-1)/2)])

        if 'yaxis' not in layout:
            layout['yaxis'] = dict(title = legend[0])

        layout['yaxis']['anchor'] = 'free'
        layout['yaxis']['side'] = 'left'
        layout['yaxis']['position'] = 0.0

        for i, _data in enumerate(data[1:]):
            layout['yaxis{}'.format(i+2)] = dict(title=legend[i+1], anchor='free', overlaying='y', side='right' if i%2==0 else 'left',
                                                 position=1-0.1*(np.int(i/2)+i%2) if i%2==0 else 0.07*(np.int(i/2)+i%2))

    fig = go.Figure()
    fig.add_traces(traces)
    fig.update_layout(layout)
    fig.write_html(str(filename), auto_open=auto_open)


def plotlyheatmap(input, filename="output.html", layout=None, auto_open=False):
    '''
    Todo
    Plots and saves data to a plotly html file.
    Unless specified, otherwise automatically chooses between a line plot or heatmap depending on the input.

    :param input:    Heatmap:    One list that can be converted to a 2d matrix
                                Three lists (z, x, y)
    :param filename:
    :param layout: Either "plotly go.Layout object" or a dict {"title":"title", "xlabel":"foo", "ylabel":"bar"}
    :param auto_open:
    :return:
    '''

    if isinstance(input[0][0], numbers.Number):
        z = np.array(input)
        x = None; y = None
    else:
        z = np.array(input[0])
        x = input[1]
        y = input[2]

    trace = go.Heatmap(z=z, x=x, y=y)

    if isinstance(layout, dict):
        layout = dict(title=layout['title'], xaxis = dict(title = layout['xlabel']), yaxis = dict(title = layout['ylabel']))

    fig = go.Figure()
    fig.add_trace(trace)
    fig.update_layout(layout)
    fig.write_html(filename, auto_open=auto_open)


class PlotlyLogger(object):
    """
    Example:
    	>>> logger = plot.PlotlyLogger()

	    >>> x = np.array(range(100))
	    >>> y1 = np.random.rand((100))
	    >>> y2 = np.random.rand((100))
	    >>> y3 = np.random.rand((50))
        >>> for _y1, _y2 in zip(y1, y2):
		>>>     logger.update(["y1", "y2"], [_y1, _y2])
	    >>> logger.update("y3", y3)
	    >>> logger.saveplot()
        >>> layout = {"title": "title", "xlabel": "Epoch", "ylabel": "Loss/Accu"}
        >>> logger.saveplot(x=list(range(100, 200)), layout=layout)
    """

    def __init__(self, opts=None, normalize=False, output="output.html"):
        self.lines = {}
        self.opts = opts
        self.normalize = normalize
        self.output = output

    def update(self, varnames, values):
        if (not isinstance(varnames, list)) and (not isinstance(varnames, tuple)):
            if varnames in self.lines:
                if isinstance(values, np.ndarray) or isinstance(values, list) or isinstance(values, tuple):
                    self.lines[varnames].extend(values)
                else:
                    self.lines[varnames].append(values)
            else:
                if isinstance(values, np.ndarray) or isinstance(values, list) or isinstance(values, tuple):
                    self.lines[varnames] = np.array(values).tolist()
                else:
                    self.lines[varnames] = [values]
        else:
            for varname, value in zip(varnames, values):
                if varname in self.lines:
                    if isinstance(value, np.ndarray) or isinstance(value, list) or isinstance(value, tuple):
                        self.lines[varname].extend(value)
                    else:
                        self.lines[varname].append(value)
                else:
                    if isinstance(value, np.ndarray) or isinstance(value, list) or isinstance(value, tuple):
                        self.lines[varname] = np.array(value).tolist()
                    else:
                        self.lines[varname] = [value]

    def saveplot(self, output=None, x=None, normalize=None, **kwargs):
        """
        :param output: path to html file. Uses default as specified in constructor if not provided.
        :param x:
        :param normalize:
        :param kwargs:  Various options such as:
                        layout: layout dict as used in plotly. eg.
                        >>> layout = {"title": "title", "xlabel": "Epoch", "ylabel": "Loss/Accu"}
        :return:
        """

        lines = {}

        if normalize is None:
            normalize = self.normalize
            
        if isinstance(normalize, list):
            for varname, norm in zip(self.lines, normalize):
                if norm:
                    lines[varname] = (np.array(self.lines[varname]) - np.min(self.lines[varname])) / (np.max(self.lines[varname]) - np.min(self.lines[varname]) + 1e-32)
                else:
                    lines[varname] = np.array(self.lines[varname])
        elif normalize:
            for varname in self.lines:
                lines[varname] = (np.array(self.lines[varname]) - np.min(self.lines[varname])) / (np.max(self.lines[varname]) - np.min(self.lines[varname]) + 1e-32)
        else:
            for varname in self.lines:
                lines[varname] = np.array(self.lines[varname])

        if output is None:
            output = self.output

        ys = [lines[varname] for varname in lines]

        if x is None:
            x = [list(range(len(y))) for y in ys]
        elif (not isinstance(x[0], list)) or (not isinstance(x[0], np.ndarray)) or (not isinstance(x[0], tuple)):
            x = [x]
        elif isinstance(x, dict):
            x = [x[varname] for varname in lines]

        default_legend = [varname for varname in lines]
        if self.opts is None:
            plotlyline([x, ys], filename=output,
                       layout=None if "layout" not in kwargs else kwargs['layout'],
                       legend=default_legend if "legend" not in kwargs else kwargs['legend'],
                       mode='lines' if "mode" not in kwargs else kwargs['mode'],
                       auto_open=False if "auto_open" not in kwargs else kwargs['auto_open'],
                       multi_ax=False if "multi_ax" not in kwargs else kwargs['multi_ax'],
                       line_colors=None if "line_colors" not in kwargs else kwargs['line_colors'])
        else:
            self.opts.default("legend", default_legend)
            plotlyline([x, ys], output=output, **{**self.opts, **kwargs}) # Values from kwargs overwrites self.opts.

    def savecsv(self, output="log.csv", x=None):
        """
        Saves data as csv. Only works if all ys have the same length.
        :param output: 
        :return: 
        """
        lines = {}

        for varname in self.lines:
            lines[varname] = np.array(self.lines[varname])

        output = Path(self.output).parent / output
        ys = [lines[varname] for varname in lines]
        x = list(range(len(ys[0])))
        keys = [varname for varname in lines]
        np.savetxt(output, np.concatenate(([x], ys), axis=0).T, delimiter="\t", header="\t".join((["#"] + keys)), comments="")

    def loadcsv(self, data, header=None):
        """
        Data can either be a path or a numpy array.
        :param data:
        :return:
        """

        if isinstance(data, str) or isinstance(data, pathlib.PosixPath):
            csvio = file_io.CSVIO(filename=data)
            data = csvio.read()
            header = data[0][0].split("\t")[1:]
            values = [[float(x) for x in line[0].split("\t")] for line in data[1:]]
            values = np.array(values)[:,1:]
        else:
            values = np.array(data)

        self.lines = {}

        if len(header) != values.shape[0]:
            values = values.T
            assert(len(header) == values.shape[0])

        self.update(header, values)


def boxswarmplot(df, x, y, order=None, title=None, plottype="boxswarm", figsize=(10, 8), dpi=100, boxopts={}, vioopts={}, swarmopts={}):
    fig = plt.figure(figsize=figsize, dpi=dpi)
    handles = {"box": None, "violin": None, "swarm": None}
    if "box" in plottype:
        handles["box"] = sns.boxplot(x=x, y=y, data=df, order=order, **boxopts)
    if "vio" in plottype:
        handles["violin"] = sns.violinplot(x=x, y=y, data=df, order=order, **vioopts)
    if "swarm" in plottype:
        if ("color" not in swarmopts) and ("hue" not in swarmopts):
            swarmopts["color"] = ".2"
        handles["swarm"] = sns.swarmplot(x=x, y=y, data=df, order=order, **swarmopts)
    new_xticks = ["{}\nn={}".format(_x.get_text(),
                                    df[df[x].astype(str) == _x.get_text()].shape[0])
                  for i, _x in enumerate(plt.xticks()[1])]
    plt.xticks(plt.xticks()[0], new_xticks, rotation=90)
    if title is not None:
        plt.title(title)

    return fig, handles


class MinorSymLogLocator(Locator):
    """
    https://stackoverflow.com/questions/20470892/how-to-place-minor-ticks-on-symlog-scale
    Dynamically find minor tick positions based on the positions of
    major ticks for a symlog scaling.

    >>> plt.plot(x, y)
    >>> plt.yscale('symlog', linthreshy=1e-1)
    >>> yaxis = plt.gca().yaxis
    >>> yaxis.set_minor_locator(MinorSymLogLocator(1e-1))
    """
    def __init__(self, linthresh):
        """
        Ticks will be placed between the major ticks.
        The placement is linear for x between -linthresh and linthresh,
        otherwise its logarithmically
        """
        self.linthresh = linthresh

    def __call__(self):
        'Return the locations of the ticks'
        majorlocs = self.axis.get_majorticklocs()

        # iterate through minor locs
        minorlocs = []

        # handle the lowest part
        for i in range(1, len(majorlocs)):
            majorstep = majorlocs[i] - majorlocs[i-1]
            if abs(majorlocs[i-1] + majorstep/2) < self.linthresh:
                ndivs = 10
            else:
                ndivs = 9
            minorstep = majorstep / ndivs
            locs = np.arange(majorlocs[i-1], majorlocs[i], minorstep)[1:]
            minorlocs.extend(locs)

        return self.raise_if_exceeds(np.array(minorlocs))

    def tick_values(self, vmin, vmax):
        raise NotImplementedError('Cannot get tick locations for a '
                                  '%s type.' % type(self))


@debug.deprecated("mosaic has been moved from plot to module image")
def mosaic(*args, **kwargs):
    return image.mosaic(*args, **kwargs)
