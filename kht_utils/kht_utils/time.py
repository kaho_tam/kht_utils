import time
import datetime
from functools import wraps


class Tictoc(object):
    """
    Measure execution time of a block of code. Can also be used to wrap functions.
    Example:
        >>>     tictoc = Tictoc():
        >>> print("Doing something here!")
        >>> tictoc.toctic  #calls tictoc.toc and then tictoc.tic to reset the clock.
        >>> print("Doing second thing")
        >>> tictoc.toc

    Wrapper Example:
        >>> @Tictoc()
        >>> def fn(x):
        >>>     [i for i in range(x)]
        >>>     return 0
    """
    def __init__(self, printtime=True):
        self.time = time.time()
        self.printtime = printtime

    @property
    def tic(self):
        self.time = time.time()
        return self.time

    @property
    def toc(self):
        time_toc = time.time()-self.time
        if self.printtime: print("Time Elapsed: {}s".format(time_toc))
        return time_toc

    @property
    def toctic(self):               # toc and then tic
        toc = self.toc
        self.time = time.time()
        return toc

    # Can also be used as a wrapper
    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            start = time.time()
            ret = fn(*args, **kwargs)
            lapse = time.time() - start
            if self.printtime: print("The function '{}' took {:.2e}s to execute".format(fn.__name__, lapse))
            return ret
        return wrapper


def datetimestring(datetimeformat="{:%y%m%d%H%M}"):
    """
    Returns a string of YYMMDDHHMM
    :param datetimeformat: "{:%y%m%d%H%M}"
    :return:
    """
    return datetimeformat.format(datetime.datetime.now())


def timeit(fn, *args, n_iter=100, verbose=True, **kwargs):

    tictoc = Tictoc(printtime=False)

    for i in range(n_iter):
        fn(*args, **kwargs)

    t = tictoc.toc

    if verbose:
        print("Total time for execution: {:.3e}s, Average time: {:.3e}s".format(t, t/n_iter))

    return t/n_iter
