import itertools
import json
import csv
import io
import re
import os
import subprocess
from datetime import datetime
from pathlib import Path
import unicodedata
import string

import scipy.io as sio
from PIL import Image
import numpy as np

try:
    import h5py
except Exception as e:
    pass
try:
    import pickle
except Exception as e:
    pass
try:
    import pyperclip
except Exception as e:
    pass


def savemat(filename="data.mat", varnames=None, *args):
    """
    Function for saving ndarray into .mat to be loaded into Matlab
    """
    dict = {}
    for i in range(len(args)):

        # Trying to convert data types to format that can be saved into .mat
        var = args[i]
        try:
            if not isinstance(var, np.ndarray):
                var = var.data
        except Exception as e:
            pass
        try:
            var = var.cpu()
        except Exception as e:
            pass
        try:
            var = var.numpy()
        except Exception as e:
            pass
        try:
            if isinstance(var, Image.Image):
                var = np.array(var)
        except Exception as e:
            pass

        if varnames is None:
            dict['var{}'.format(i)] = var
        else:
            try:
                dict[varnames[i]] = var
            except Exception as e:
                dict['var{}'.format(i)] = var

    try:
        sio.savemat(filename, dict)
    except Exception as e:
        print("Error saving .mat file: ", e)


def h5io(filename="data.h5", *args, varnames=[], mode='w'):
    """
    Function for saving / loading ndarrays into .h5 files
    """
    if len(args) == 0:

        if not Path(filename).exists():
            raise FileNotFoundError

        with h5py.File(filename, 'r') as f:
            # List all groups
            data = {}
            if len(varnames) == 0:
                varnames = f.keys()
            for key in varnames:
                data[key] = list(f[key])
                
            return data

    dict = {}
    for i in range(len(args)):

        # Trying to convert data types to format that can be saved into .mat
        var = args[i]
        try:
            if not isinstance(var, np.ndarray):
                var = var.data
        except AttributeError:
            pass
        try:
            var = var.cpu()
        except AttributeError:
            pass
        try:
            var = var.numpy()
        except AttributeError:
            pass
        try:
            if isinstance(var, Image.Image):
                var = np.array(var)
        except Exception as e:
            pass

        if varnames == []:
            dict['var{}'.format(i)] = var
        else:
            try:
                dict[varnames[i]] = var
            except Exception as e:
                dict['var{}'.format(i)] = var

    if mode == 'w':
        try:
            hf = h5py.File(filename, 'w')
            for key in dict:
                hf.create_dataset(key, data=dict[key])
            hf.close()
        except Exception as e:
            print("Error saving .h5 file: ", e)
    else:
        with h5py.File(filename, 'a') as hf:
            for key in dict:
                hf[key] = dict[key]


def clipboard_list(arr):
    """
    Puts a list to the system's clipboard.
    :param arr: a list of variables that can be casted into string or a list of list containing variables that can be casted as strings.
    """
    if not isinstance(arr[0], list):
        arr = [str(x) for x in arr]
    else:
        arr_new = []
        for _arr in itertools.zip_longest(*arr, fillvalue=''):
            arr_new.append('\t'.join([str(_cell) for _cell in _arr]))
        arr = arr_new

    arr = "\n".join(arr)
    pyperclip.copy(arr)


def clipboard_paste(delim="\n"):
    text = pyperclip.paste()
    if text.startswith("[") or text.startswith("(") or text.startswith("{"):
        try:
            return eval(text)
        except Exception as e:
            return text.split(delim)
    else:
        return text.split(delim)


def json_io(filename, data=None, sort_keys=True):
    """
    Saves and loads dict to/from json (depending on whether data is None)
    :param filename: filename to save to.
    :param data: dict to be saved as json
    :param sort_keys:   bool, whether to sort the keys in the json file.
                        Only works if order of all keys can be compared.
    """
    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    if data is None:
        # Read JSON file
        with open(filename) as data_file:
            data = json.load(data_file)
        return data
    else:
        # Write JSON file
        with io.open(filename, 'w', encoding='utf8') as outfile:
            str_ = json.dumps(data,
                              indent=4, sort_keys=sort_keys, cls=NumpyEncoder,
                              separators=(',', ': '), ensure_ascii=False)
            outfile.write(to_unicode(str_))
        return 0


class CSVIO(object):
    """
    Class for input/output to/from csv files
    """

    def __init__(self, filename='debug.csv', data=None):
        self.filename = filename
        if data is not None:
            self.write(data[0], mode='w')
            for line in data[1:]:
                self.write(line)

    def read(self):  # read the whole file and return a list
        with open(self.filename) as f:
            reader = csv.reader(f)
            data = [row for row in reader]
            return data

    def readline(self):  # read file line by line. returns a generator
        with open(self.filename) as f:
            reader = csv.reader(f)
            for row in reader:
                yield row

    def readdict(self):  # returns a generator of dict
        with open(self.filename) as f:
            reader = csv.DictReader(f)
            for row in reader:
                yield row

    def write(self, *args, mode='a'):

        if not Path(self.filename).parent.exists():
            os.makedirs(Path(self.filename).parent)

        with open(self.filename, mode) as f:
            writer = csv.writer(f)
            for arg in args:
                writer.writerow(arg)


class NumpyEncoder(json.JSONEncoder):
    """
    Cleans up objects inside a dict so that it becomes "json serializable"
    https://stackoverflow.com/questions/26646362/numpy-array-is-not-json-serializable/32850511?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
    Special json encoder for numpy types
    """

    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):  #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def pickle_io(filename, data=None):
    """
    Saves and loads objects using pickle
    """
    filename = str(filename)
    if "." not in filename[-4:]:
        filename = filename + ".pkl"

    if data is None:
        try:
            data = pickle.load(open(filename, "rb"))
            return data
        except Exception as e:
            print("Cannot load file {}, {}".format(filename, e))
            return False
    else:
        pickle.dump(data, open(filename, "wb"))


def findbydate(path, pattern, date='2999-12-31-23:59:59', mode='latest', return_date=False):
    """
    Find the file matching pattern closest/immediately b4/after to the specified modified date.

    :param path: libpath Path object
    :param pattern: regex of filepath
    :param date: date in iso format
    :param mode: 'closest': returns file path with closest absolute distance from the date given
                 'before': returns file path with modification date closest and before the date given
                 'after': returns file path with modification date closest and after the date given
                 'latest': returns the file path with latest modification date
                 'earliest': returns the file path with earliest modification date
    :param return_date: whether to return only the filepath or the date as well
    
    Example:
    >>> path = "./"
    >>> pattern = r'*temp*'
    >>> filepath1, date1 = file_io.findbydate(path, pattern, return_date=True)
    """

    path = Path(path)
    assert mode in ['earliest', 'latest', 'before', 'after']

    date = datetime.timestamp(datetime.fromisoformat(date))

    filepath = None

    if mode.lower() == 'before':
        try:
            _, time, filepath = max((f.stat().st_mtime - date, f.stat().st_mtime, f) for f in path.rglob(pattern) if
                                    f.stat().st_mtime < date)
        except ValueError:
            pass
    if mode.lower() == "after":
        try:
            _, time, filepath = min((f.stat().st_mtime - date, f.stat().st_mtime, f) for f in path.rglob(pattern) if
                                    f.stat().st_mtime > date)
        except ValueError:
            pass

    if "earl" in mode.lower():
        try:
            filelist = []
            for f in path.rglob(pattern):
                try:
                    filelist.append((abs(date - f.stat().st_mtime), f.stat().st_mtime, f))
                except FileNotFoundError:
                    pass

            _, time, filepath = max(filelist)
        except ValueError:
            pass

    if mode.lower() == 'closest' or (filepath is None):
        try:
            filelist = []
            for f in path.rglob(pattern):
                try:
                    filelist.append((abs(date - f.stat().st_mtime), f.stat().st_mtime, f))
                except FileNotFoundError:
                    pass

            _, time, filepath = min(filelist)
        except ValueError:
            pass

    if return_date:
        return filepath, datetime.fromtimestamp(time)
    else:
        return filepath


def git_hash(short=True):
    """
    Returns hash of current git commit version in current directory. Useful for logging.
    :param short: returns short hash
    :return: hash
    """
    if short:
        return subprocess.check_output(['git', 'rev-parse', 'HEAD'])
    else:
        return subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])


def tree(directory=Path('./'), dlim=0, flim=0):
    """
    Print file tree
    :param directory: pathlib directory
    :param dlim: If >0, limits the number of dir printed in each subdirectory.
    :param flim: If >0, limits the number of files printed in each subdirectory.
    """

    print(f'+ {directory}')
    if dlim > 0 or flim > 0:
        printed_list = [directory]
        dirfile_count = {}

    for path in sorted(directory.rglob('*')):
        depth = len(path.relative_to(directory).parts)
        spacer = '    ' * depth

        if dlim > 0 or flim > 0:
            if path.parent not in dirfile_count:
                dirfile_count[path.parent] = [0, 0]
            if (path.parent not in printed_list and path != directory):
                continue
            if path.is_file():
                if dirfile_count[path.parent][1] >= flim:
                    continue
                dirfile_count[path.parent][1] += 1
            else:
                if dirfile_count[path.parent][0] >= dlim:
                    continue
                dirfile_count[path.parent][0] += 1

        print(f"{spacer}{'-' if path.is_file() else '+'} {path.name}")
        printed_list.append(path)


def rglob(rootdir=Path.cwd(), pattern="*", regex_mode=True, ignore_case=False,
          recursive=False, exclude=None, return_groups=False, relative_path=False,
          walksymlink=False):
    """
    Generates a list of directories matching "pattern". More robust than pathlib's rglob in the following ways:
    1) Option to ignore case (ignore_case, defaults to regex matching mode)
    2) Regex matching mode
    2) In regex "*" means matching 0 or more. In this function "*" will remain a wildcard.
        However there is an option to define whether "*" queries searches includes "/"
    :param rootdir:
    :param pattern:
    :param regex_mode: Regex mode. However * is being interpreted as any char (other than "/") unless specified otherwise..
    :param ignore_case: Ignore case. Automatically changes the search to regex mode
    :param recursive: Wildcard includes the character "/" as well? Enabling will give more results at greater depths.
    :param exclude: pattern to be excluded in results (in regex mode)
    :param asterisk_slash: Wildcard includes the character "/" as well? Enabling will give more results at greater depths.
    :param return_groups: returns capture groups as specified by the regex pattern
    :param relative_path: returns path name relative to rootdir instead of the full path
    :param walksymlink: whether to walk through directories which are symlinks. Warning! May cause PC to freeze if there are loops in the links.
    :return: generator for path names (regex match groups)
    """

    rootdir = Path() / rootdir

    if regex_mode or ignore_case:

        if not recursive:
            _pattern = str(pattern).replace("*", "[^/]*") + "$"
            dirs = rootdir.glob("/".join(["*"] * (list.count([char for char in str(pattern)], "/") + 1)))
        else:
            _pattern = str(pattern).replace("*", "[\w|\W]*")
            dirs = rootdir.rglob('*')

        if ignore_case:
            _pattern = re.compile(_pattern, re.IGNORECASE)
        else:
            _pattern = re.compile(_pattern)

        if exclude is None:
            for _dir in dirs:

                # Pathlib's rglob doesn't walk directories which are symlink which is quite annoying.
                # Note the below statement may be buggy
                if walksymlink and _dir.is_symlink():
                    if not recursive:
                        nested_pattern = "/".join(str(pattern).split("/")[len(_dir.relative_to(rootdir).parts):])
                    else:
                        nested_pattern = pattern    # Todo need more checking.
                    if nested_pattern != '':
                        nested_results = rglob(rootdir=_dir, pattern=nested_pattern, regex_mode=True, ignore_case=ignore_case, recursive=recursive,
                                               exclude=exclude, return_groups=return_groups, relative_path=relative_path, walksymlink=walksymlink)
                        for nested_result in nested_results:
                            yield nested_result

                searched = re.search(_pattern, str(_dir.relative_to(rootdir)))
                _dir = _dir.relative_to(rootdir) if relative_path else _dir
                if (searched is not None) and return_groups:
                    yield _dir, searched.groups()
                elif (searched is not None):
                    yield _dir
        else:
            if ignore_case:
                exclude = re.compile(exclude, re.IGNORECASE)
            else:
                exclude = re.compile(exclude)
            for _dir in dirs:
                
                # Pathlib's rglob doesn't walk directories which are symlink which is quite annoying.
                # Note the below statement may be buggy
                if _dir.is_symlink() and walksymlink:
                    if not recursive:
                        nested_pattern = "/".join(str(pattern).split("/")[len(_dir.relative_to(rootdir).parts):])
                    else:
                        nested_pattern = pattern    # Todo need more checking.
                    if nested_pattern != '':
                        nested_results = rglob(rootdir=_dir, pattern=nested_pattern, regex_mode=True, ignore_case=ignore_case, recursive=recursive,
                                               exclude=exclude, return_groups=return_groups, relative_path=relative_path, walksymlink=walksymlink)
                        for nested_result in nested_results:
                            yield nested_result
                        
                searched = re.search(_pattern, str(_dir.relative_to(rootdir)))
                excluded_searched = re.search(exclude, str(_dir.relative_to(rootdir)))
                _dir = _dir.relative_to(rootdir) if relative_path else _dir
                if (searched is not None) and (excluded_searched is None) and return_groups:
                    yield _dir, searched.groups()
                elif (searched is not None) and (excluded_searched is None):
                    yield _dir

    else:
        if recursive:
            dirs = rootdir.rglob(pattern)
        else:
            dirs = rootdir.glob(pattern)
        for _dir in dirs:
            _dir = _dir.relative_to(rootdir) if relative_path else _dir
            yield _dir


def rlink(src, dst=None, is_hard=False, root=True, warn_missing=True):
    """
    Creates relative symbolink between src and dst

    :param src:
    :param dst:
    :param is_hard:
    :param root:
    :param warn_missing: Todo: print message if dst is missing.
    :return:
    """

    if dst is None:
        dst = Path(src).name

    if isinstance(src, str) and ("~" in src):
        src = src.replace("~", str(Path.home()))
    if isinstance(dst, str) and ("~" in dst):
        dst = dst.replace("~", str(Path.home()))

    if root:
        while os.path.islink(src):
            src = Path(src).parent / os.readlink(src)

    if not Path(src).exists():
        print("Warning: src {} is missing".format(src))

    src = Path(src).absolute()
    dst = Path(dst).absolute()
    src = os.path.relpath(src, dst.parent)

    os.remove(dst) if dst.exists() else None
    if is_hard:
        os.link(src, dst)
    else:
        os.symlink(src, dst)


def replace_symlink(filepath="./", pattern="*", find="", replace="", relative=True, broken_only=True, log=False, root=True):
    """
    Find and replace substring in symlink. Replace with relative link.
    :param filepath: File Path (regex)
    :param find: Find string (regex)
    :param replace: Replace with (regex)
    :param relative: Relative path?
    :param broken_only: replace broken symlinks only.
    :param log: write broken links to log.txt
    :param root: if target is also a symlink, link to the target's target.
    :return: 
    """

    filepath = Path(filepath)
    if log:
        f = open("log.txt", "a+")

    for _filepath in rglob(filepath, pattern=pattern):

        msg = str(_filepath) + "\n"

        if not os.path.islink(_filepath):
            continue

        link = os.readlink(_filepath)

        if (_filepath.parent / link).exists() and broken_only:
            continue

        if (_filepath.parent / link).exists():
            os.remove(_filepath)
            rlink(_filepath.parent / link, _filepath, root=root)
            print(f"Replaced {_filepath}")
            continue
        else:
            msg += "{} or \n{} does not exist.\n".format(link, Path(link).absolute())

        replaced = re.sub(find, replace, str(_filepath.parent / link))

        if Path(replaced).exists():
            os.remove(_filepath)
            if relative:
                rlink(replaced, _filepath, root=root)
            else:
                os.symlink(replaced, _filepath)
            print(f"Replaced {_filepath}")
            continue
        else:
            replaced = re.sub(find, replace, str(link))
            if Path(replaced).exists():
                os.remove(_filepath)
                if relative:
                    rlink(replaced, _filepath, root=root)
                else:
                    os.symlink(replaced, _filepath)
                print(f"Replaced {_filepath}")
                continue
            else:
                msg += "{} or \n {} does not exist either.\n\n".format(replaced, Path(replaced).absolute())

        print(msg)

        if log:
            f.write(str(_filepath) + "\n")

    if log:
        f.close()


def merge_symlink(target, sources, pattern="*", overwrite=True, remove_empty_dir=True, verbose=False):
    """
    Use: python merge_symlink(target, (source1, source2, source3, ...), overwrite=True, remove_empty_dir=True, verbose=false)

    Note that if overwrite==True and there are duplicated filenames, links will be overwritten by the last argument's
    See https://askubuntu.com/questions/1097502/script-for-merging-files-in-multiple-nested-directories-by-symbolic-link/
    Function to be run in the target directory.

    :param target:
    :param sources: a list of directories where the files in the subdirectories are to be merged symbolically. Path relative to the target. eg. ["../folder1", "../folder2"]
    :param pattern: regex pattern. See kht.file_ioZ.rglob
    :param overwrite: Bool, whether to overwrite existing symbolic links
    :param remove_empty_dir: Bool, whether to remove empty directories in target.
    :param verbose: Prints stuff.
    :return: None
    """

    target = Path(target)

    # Creating symlinks and folders
    for source in sources:

        for path in rglob(source, pattern=pattern, recursive=True, walksymlink=True):

            if path.is_dir():
                if not (target / path.relative_to(source)).exists():
                    os.makedirs(target / path.relative_to(source))
            else:
                targetlink = target / path.relative_to(source)
                if not targetlink.parent.exists():
                    os.makedirs(targetlink.parent)
                if (not targetlink.exists()) and isvalidlink(path) != 0:
                    rlink(path, targetlink)
                elif overwrite and (not isvalidlink(targetlink) == 2) and isvalidlink(path) != 0:  # Never replace a real file with a symlink!
                    if verbose:
                        print('overwriting {}'.format(targetlink))
                    rlink(path, targetlink)

    # Pruning broken links and then deleting empty folders.
    for path in rglob(target, pattern=pattern, recursive=True, walksymlink=True):
        if isvalidlink(path) == 0:
            os.remove(path)
            if verbose:
                print("Removing broken symlink: {}".format(path))

    if remove_empty_dir:
        for path in rglob(target, pattern="*", recursive=True, walksymlink=True):
            if path.is_dir() and os.listdir(path) == []:
                os.rmdir(path)


def isvalidlink(path):
    """
    Checks if file is a broken link. 0: broken link; 1: valid link; 2: not a link
    :param path:
    :return:
    """
    if not os.path.islink(path):
        return 2
    try:
        os.stat(path)
    except os.error:
        return 0
    return 1


def clean_filename(filename, whitelist="-_.() %s%s" % (string.ascii_letters, string.digits), replace=' ', char_limit=255):
    # replace spaces
    for r in replace:
        filename = filename.replace(r,'_')
    
    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore').decode()
    
    # keep only whitelisted chars
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename)>char_limit:
        print("Warning, filename truncated because it was over {}. Filenames may no longer be unique".format(char_limit))
    return cleaned_filename[:char_limit]   
