name = "kht_utils"
try:
    import kht_utils.debug
except Exception as e:
    print("Cannot Import kht_utils.debug")
try:
    import kht_utils.file_io
except Exception as e:
    print("Cannot Import kht_utils.file_io")
try:
    import kht_utils.numeric
except Exception as e:
    print("Cannot Import kht_utils.numeric")
try:
    import kht_utils.plot
except Exception as e:
    print("Cannot Import kht_utils.plot")
try:
    import kht_utils.image
except Exception as e:
    print("Cannot Import kht_utils.image")
try:
    import kht_utils.time
except Exception as e:
    print("Cannot Import kht_utils.time")
try:
    import kht_utils.vartype
except Exception as e:
    print("Cannot Import kht_utils.vartype")
