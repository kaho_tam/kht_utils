import numpy as np
import scipy

class Average(object):
    """
    Average class to accumulate values and calculate averages.
    Set store=n to storage values and calculates running mean.
    """
    def __init__(self, store=0):
        self.reset()
        self.store = store
        self.history = []

    def reset(self):
        self.sum = 0
        self.count = 0
        self.history = []

    def update(self, val, n=1):
        self.last = self.avg
        self.sum += val
        self.count += n
        if self.store:
            self.history.append(val)
            while (len(self.history) > self.store):
                self.sum -= self.history.pop(0)
                self.count -= 1

    @property
    def avg(self):
        try:
            return self.sum / (self.count + np.finfo(np.float64).eps)
        except ZeroDivisionError:
            return 0

    def __itruediv__(self, scalar):
        for i, _ in enumerate(self.history):
            self.history[i] /= scalar
        self.sum /= scalar
        self.last /= scalar
        return self

    def __imul__(self, scalar):
        for i, _ in enumerate(self.history):
            self.history[i] *= scalar
        self.sum *= scalar
        self.last *= scalar
        return self


def normalize(arr, norm_min=0.0, norm_max=1.0, map_min=None, map_max=None):
    """
    Normalise array such that its values are between norm_min and norm_max
    :param arr:
    :param norm_max:
    :param norm_min:
    :param map_min: If given, any values below map_min is mapped to norm_min
    :param map_max: If given, any values above map_max is mapped to norm_max
    """
    
    assert norm_max > norm_min
    
    if (map_min is not None) and (map_max is not None):
        assert map_max > map_min
        
    if map_min is not None:
        arr = np.maximum(map_min, arr)
        
    if map_max is not None:
        arr = np.minimum(map_max, arr)
    
    arr_min = np.min(arr)
    arr_max = np.max(arr)
    
    if arr_min == arr_max:
        arr_min -= 1e-9

    return (arr-arr_min)/(arr_max-arr_min) * (norm_max-norm_min) - norm_min


def symlog(x, inverse=False, C=1.0):
    """
    Symmetric Log Function
    * A bi-symmetric log transformation for wide-range data *
    :param x:
    :param inverse:
    :param C:  A constant that adjusts the slope of the transfer function near the origin
    :return:
    """
    if inverse:
        y = np.sign(x) * C * (-1 + np.power(10, np.abs(x)))
    else:
        y = np.sign(x) * np.log10(1 + np.abs(x/C))

    return y


def bit_reverse(img, out_bits=16):
    """
    Reverses the bitwise representation of 16-bit numpy arrays.
    :param img:
    :return:
    """

    if out_bits==16:
        out = np.zeros(img.shape, dtype=np.uint16)
    else:
        out = np.zeros(img.shape, dtype=np.uint8)

    for i in range(out_bits):
        _img = img & (1 << i)
        if i < (out_bits // 2):
            out += _img << (out_bits - 1 - 2*i)
        else:
            out += _img >> (2*i - out_bits + 1)

    return out


def moving_avg(array_numbers, n):
    if n > len(array_numbers):
      return np.array([])
    temp_sum = sum(array_numbers[:n])
    averages = [temp_sum / float(n)]
    for first_index, item in enumerate(array_numbers[n:]):
        temp_sum += item - array_numbers[first_index]
        averages.append(temp_sum / float(n))
    return np.array(averages)


def pearsonr_ci(x,y,alpha=0.05):
    ''' calculate Pearson correlation along with the confidence interval using scipy and numpy
    https://zhiyzuo.github.io/Pearson-Correlation-CI-in-Python/
    Parameters
    ----------
    x, y : iterable object such as a list or np.array
      Input for correlation calculation
    alpha : float
      Significance level. 0.05 by default
    Returns
    -------
    r : float
      Pearson's correlation coefficient
    pval : float
      The corresponding p value
    lo, hi : float
      The lower and upper bound of confidence intervals
    '''

    r, p = scipy.stats.pearsonr(x,y)
    r_z = np.arctanh(r)
    se = 1/np.sqrt(x.size-3)
    z = scipy.stats.norm.ppf(1-alpha/2)
    lo_z, hi_z = r_z-z*se, r_z+z*se
    lo, hi = np.tanh((lo_z, hi_z))
    return r, p, lo, hi