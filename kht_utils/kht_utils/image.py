import sys
from tempfile import SpooledTemporaryFile
from multiprocessing import Process, Queue
from pathlib import Path
import numpy as np
import math
from skimage.filters import sobel
from scipy.ndimage.morphology import distance_transform_edt
from scipy import signal
from PIL import Image
import pyvips
import torch
import rpack
try:
    from . import numeric
except:
    import numeric
try:
    from . import numeric
except:
    import numeric


def mosaic(*imgs, nrows=0, ncols=0, min_perimeter=False, orientation=None, bgval=0):
    """
    Tile up list of images

    :param imgs:
    :param ncols:
    :param nrows:
    :param min_perimeter: Minimize the perimeter of the mosaic by tiling the images to as close to a square as possible.
    :param orientation: l/p/None Whether to rotate the image to fit landscape/portrait.
    :return:
    """

    imgs = list(imgs)
    nplots = len(imgs)

    for i, img in enumerate(imgs):
        imgs[i] = np.array(img)

    heights = [img.shape[0] for img in imgs]
    widths = [img.shape[1] for img in imgs]
    channels = [img.shape[2] if len(img.shape) > 2 else 1 for img in imgs]
    max_ch = np.max(channels)
    use_rect_packer = not ((np.array(heights).max() == np.array(heights).min())
                           and (np.array(widths).max() == np.array(widths).min()))

    if max_ch > 1:
        for i, chs in enumerate(channels):
            if chs == 1:
                imgs[i] = np.tile(imgs[i][:, :, np.newaxis], (1, 1, max_ch))

    if use_rect_packer:
        sizes = list(zip(widths, heights))

        # Determines max width
        area_sqrt = np.sqrt(np.array(sizes).prod(axis=1).sum())
        sizes_sorted = sorted(sizes)
        w_sorted_inv = np.array(list(reversed(sizes_sorted)))[:,0]
        w_sorted = np.array(sizes_sorted)[:,0]
        w_zipped = np.stack((w_sorted_inv, w_sorted)).T.ravel()
        w_zipped_sum = np.cumsum(w_zipped)
        max_width = int(w_zipped_sum[np.argmax(w_zipped_sum >= area_sqrt)])

        h_sorted_inv = np.array(list(reversed(sizes_sorted)))[:,1]
        h_sorted = np.array(sizes_sorted)[:,1]
        h_zipped = np.stack((h_sorted_inv, h_sorted)).T.ravel()
        h_zipped_sum = np.cumsum(h_zipped)
        max_height = int(h_zipped_sum[np.argmax(h_zipped_sum >= area_sqrt)])

        positions = rpack.pack(sizes, max_width=int(max_width*1.4), max_height=int(max_height*1.4))
        positions_end = [(p[0] + w, p[1] + h) for p, w, h in zip(positions, widths, heights)]
        w, h = np.max(positions_end, axis=0)
        if max_ch > 1:
            output = np.ones((h, w, max_ch)) * bgval
        else:
            output = np.ones((h, w)) * bgval

        for i, (x, y) in enumerate(positions):
            output[y:y+imgs[i].shape[0], x:x+imgs[i].shape[1]] = imgs[i]

    else:
        if min_perimeter:
            w = imgs[0].shape[1]
            h = imgs[0].shape[0]
            best_n = 1
            best_m = nplots
            wph = nplots*h + w # width plus height
            for n in range(2, nplots+1):
                m = np.int(np.ceil(nplots / n))
                if m*h + n*w < best_m * h + best_n * w:
                    best_n = n
                    best_m = m
            nrows = best_m
            ncols = best_n

        # Want a number of subplots depending on nplot: 1, 1x2, 2x2, 2x2, 2x3, 2x3, 3x3, 3x3, 3x3, 3x4, 3x4, 3x4, 4x4 ...
        if nrows==0 and ncols==0:
            nrows = np.int(np.ceil(0.5*(np.sqrt(1+4*nplots)-1)))
            ncols = np.int(np.ceil(np.sqrt(nplots)))
        elif nrows == 0:
            nrows = np.int(np.ceil(nplots / ncols))
        elif ncols == 0:
            ncols = np.int(np.ceil(nplots / nrows))

        if len(imgs[0].shape)==2:
            output = np.zeros((imgs[0].shape[0]*nrows, imgs[0].shape[1]*ncols))
        else:
            output = np.zeros((imgs[0].shape[0]*nrows, imgs[0].shape[1]*ncols, imgs[0].shape[2]))

        n = 0
        for i in range(ncols):
            for j in range(nrows):
                if n == len(imgs):
                    break
                output[j*imgs[0].shape[0]:j*imgs[0].shape[0] + imgs[0].shape[0], i*imgs[0].shape[1]:i*imgs[0].shape[1] + imgs[0].shape[1]] = imgs[n]
                n += 1

        if orientation is not None:
            if output.shape[0] > output.shape[1] and orientation.lower().startswith("l"):
                output = np.rot90(output)
            if output.shape[1] > output.shape[0] and orientation.lower().startswith("p"):
                output = np.rot90(output)

    return output


def overlay_mask(img, mask, touching=False, col=[0, 255, 0]):
    """
    Overlays the boundaries of the mask on top of an image..
    :param img:
    :param mask:
    :param touching: whether to show the boundaries of touching objects (Much slower if True)
    :param col: RGB value for boundaries (int 0-255)
    :return:
    """

    img = np.array(img)
    mask = np.array(mask)

    assert(len(img.shape)==3)
    assert(img.shape[:2]==mask.shape[:2])

    if len(mask.shape) == 3:
        mask = mask[:, :, 0]

    if touching:
        mask_edge = np.zeros(mask.shape, dtype=np.uint8)
        labels = list(set(mask.ravel()))
        labels.pop(0)
        for instance_idx in labels:
            mask_edge = np.maximum(mask_edge, sobel(mask == instance_idx))

    else:
        mask_edge = sobel(mask > 0)

    mask_edge = numeric.normalize(mask_edge)

    img_overlay = np.stack((col[0] * mask_edge + img[:, :, 0] * (1 - mask_edge),
                            col[1] * mask_edge + img[:, :, 1] * (1 - mask_edge),
                            col[2] * mask_edge + img[:, :, 2] * (1 - mask_edge)), axis=2)

    return img_overlay.astype(img.dtype)


def process_patches(img, fn, max_size, overlap=0, force_size=False, method='dist_mean'):
    """
    Process an image patch by patch. Patch size's upper limit is max_size + 2*overlap
    :param img:
    :param fn:
    :param max_size:
    :param overlap:
    :param force_size: Ensures that the image_size==max_size.
    :param method:
    :return:
    """

    img = np.array(img)
    nr = np.int(np.ceil(img.shape[0] / max_size))
    nc = np.int(np.ceil(img.shape[1] / max_size))
    h = np.int(img.shape[0]/nr)
    w = np.int(img.shape[1]/nc)

    if method == "mean":
        mean = numeric.Average()
        for col_idx in range(nc):
            for row_idx in range(nr):
                x_start = np.maximum(0, col_idx * w - overlap)
                x_end = np.minimum(img.shape[1], (col_idx + 1) * w + overlap)
                y_start = np.maximum(0, row_idx * h - overlap)
                y_end = np.minimum(img.shape[0], (row_idx + 1) * h + overlap)

                if force_size:
                    while (x_end-x_start) != max_size:
                        x_start = np.maximum(0, x_start - 1)
                        if (x_end-x_start) != max_size:
                            x_end = np.minimum(img.shape[1], x_end+1)
                    while (y_end-y_start) != max_size:
                        y_start = np.maximum(0, y_start - 1)
                        if (y_end-y_start) != max_size:
                            y_end = np.minimum(img.shape[0], y_end+1)

                _img = img[y_start:y_end, x_start:x_end]
                _img = fn(_img)
                if len(img.shape) > 2:
                    _img_padded = np.zeros((img.shape[0], img.shape[1], _img.shape[2]))
                    mask = np.zeros((img.shape[0], img.shape[1], _img.shape[2]))
                else:
                    _img_padded = np.zeros((img.shape[0], img.shape[1]))
                    mask = np.zeros((img.shape[0], img.shape[1]))

                mask += np.finfo(mask.dtype).eps
                mask[y_start:y_end, x_start:x_end] = 1
                _img_padded[y_start:y_end, x_start:x_end] = _img
                mean.update(_img_padded, n=mask)
        return mean.avg

    elif method == "dist_mean":
        mean = numeric.Average()
        dist_sum = np.zeros((img.shape[:2]))
        for col_idx in range(nc):
            for row_idx in range(nr):
                x_start = np.maximum(0, col_idx * w - overlap)
                x_end = np.minimum(img.shape[1], (col_idx + 1) * w + overlap)
                y_start = np.maximum(0, row_idx * h - overlap)
                y_end = np.minimum(img.shape[0], (row_idx + 1) * h + overlap)

                if force_size:
                    while (x_end-x_start) < max_size and (x_start!=0 and x_end!=img.shape[0]):
                        x_start = np.maximum(0, x_start - 1)
                        if (x_end-x_start) != max_size:
                            x_end = np.minimum(img.shape[1], x_end+1)
                    while (y_end-y_start) < max_size and (y_start!=0 and y_end!=img.shape[0]):
                        y_start = np.maximum(0, y_start - 1)
                        if (y_end-y_start) != max_size:
                            y_end = np.minimum(img.shape[0], y_end+1)

                _img = img[y_start:y_end, x_start:x_end]
                _img = fn(_img)

                if _img.shape[0] != y_end-y_start or _img.shape[1] != x_end-x_start: #and (_img.dtype==np.uint8 or _img.dtype==np.uint16):
                    dtype = _img.dtype
                    _img = Image.fromarray(_img).resize((x_end-x_start, y_end-y_start))
                    _img = np.array(_img).astype(dtype)

                dist = np.ones((_img.shape[:2]))
                dist[0, :] = 0
                dist[-1, :] = 0
                dist[:, 0] = 0
                dist[0:, -1] = 0
                dist = distance_transform_edt(dist) + 1

                if len(img.shape) > 2:
                    _img_padded = np.zeros((img.shape[0], img.shape[1], _img.shape[2]))
                    mask = np.zeros((img.shape[0], img.shape[1], _img.shape[2])) + np.finfo(np.float).eps
                    mask[y_start:y_end, x_start:x_end] = np.tile(dist[:, :, np.newaxis], (1, 1, _img.shape[2]))
                else:
                    _img_padded = np.zeros((img.shape[0], img.shape[1]))
                    mask = np.zeros((img.shape[0], img.shape[1])) + np.finfo(np.float).eps
                    mask[y_start:y_end, x_start:x_end] = dist

                dist_sum[y_start:y_end, x_start:x_end] += dist
                _img_padded[y_start:y_end, x_start:x_end] = _img
                mean.update(_img_padded * mask)

        if len(img.shape) < 3:
            return mean.sum / (dist_sum + np.finfo(np.float).eps)
        else:
            return mean.sum / (np.tile(dist_sum[:, :, np.newaxis], (1, 1, 3)) + np.finfo(np.float).eps)

    else:
        print("Please input a valid method: mean / dist_mean")


def resize(img, size=None, ratio=None):

    assert (size is not None) or (ratio is not None)

    if (size is not None) and isinstance(img, Image.Image):
        return img.resize(size)

    elif (size is not None) and isinstance(img, np.ndarray):
        dtype = _img.dtype
        print("Todo")
        _img = Image.fromarray(img)
        _img = _img.resize(size)
        _img = np.array(_img)
        return _img

    elif isinstance(img, Image.Image):
        newsize = (np.array(img.size) * ratio).astype(np.int)
        return img.resize(newsize)

    elif isinstance(img, np.ndarray):
        dtype = _img.dtype
        print("Todo")
        _img = Image.fromarray(img)
        newsize = (np.array(_img.size) * ratio).astype(np.int)
        _img = _img.resize(newsize)
        _img = np.array(_img)
        return _img


def pyvips_crop(image, x, y, w, h, pad=0):
    if x > 0 and y > 0 and x + w <= image.width and y + h <= image.height:
        image = image.crop(x, y, w, h)
    else:  # Cropping with black boarders
        black = pyvips.Image.black(w, h)
        if pad > 0:
            black = black + pad
        tile = image.crop(np.maximum(x, 0), np.maximum(y, 0),
                          np.minimum(image.width - np.maximum(x, 0), w),
                          np.minimum(image.height - np.maximum(y, 0), h))
        image = black.insert(tile, np.maximum(0, -x), np.maximum(0, -y))
    return image


def pyvips_load(slidepath, level=0, x=None, y=None, w=None, h=None, mode="default", to_arr=False):
    """
    Uses pyvips' tiffload to load a level. Reiszes the previous existing level if level doesn't exist.
    :param slidepath:
    :param level:
    :param mode: default/center. In center mode, (x,y) specifies the center coords rather than the top left
    :return:
    """
    slide = None
    lvl = level
    if "c" in mode.lower():
        x, y = int(x - w/2), int(y - h/2)
    while (slide is None) and lvl > -1:
        try:
            slide = pyvips.Image.tiffload(str(slidepath), page=lvl)
            if level != lvl:
                slide = pyvips.Image.resize(slide, np.power(0.5, level - lvl))
        except Exception as e:
            # print("Slides does not have level {}; Exception: {}".format(lvl, e))
            lvl -= 1

    # Cropping image
    try:
        assert slide is not None
    except:
        if level >= 0:
            slide = pyvips.Image.tiffload(str(slidepath), page=lvl)
            if level != lvl:
                slide = pyvips.Image.resize(slide, np.power(0.5, level - lvl))
        else:
            slide = pyvips.Image.tiffload(str(slidepath), page=0)
            slide = pyvips.Image.resize(slide, np.power(0.5, level))

    if (x is not None) and (y is not None) and (w is not None) and (h is not None):
        slide = pyvips_crop(slide, x, y, w, h)

    assert slide is not None

    if to_arr:
        return pyvips2arr(slide)
    return slide

dtype_to_format = {
    'uint8': 'uchar',
    'int8': 'char',
    'uint16': 'ushort',
    'int16': 'short',
    'uint32': 'uint',
    'int32': 'int',
    'float32': 'float',
    'float64': 'double',
    'complex64': 'complex',
    'complex128': 'dpcomplex',
}

format_to_dtype = {
    'uchar': 'uint8',
    'char': 'int8',
    'ushort': 'uint16',
    'short': 'int16',
    'uint': 'uint32',
    'int': 'int32',
    'float': 'float32',
    'double': 'float64',
    'complex': 'complex64',
    'dpcomplex': 'complex128',
}


def pyvips2arr(img):
    if isinstance(img, np.ndarray):
        return img
    buf = img.write_to_memory()
    arr = np.frombuffer(buf, dtype=format_to_dtype[str(img.format)], count=-1, offset=0).reshape(img.height, img.width, img.bands)
    return arr.squeeze()


def arr2pyvips(img):
    if str(img.dtype) not in dtype_to_format:
        img = img.astype(np.float32)
    img = pyvips.Image.new_from_memory(img.flatten().data, img.shape[1], img.shape[0],
                                       img.shape[2] if len(img.shape) > 2 else 1, dtype_to_format[str(img.dtype)])
    return img


def tiffsave(filepath, img, **kwargs):
    """
    """
    default_config = {"tile":True, "tile_width":512, "tile_height":512, "squash":False, "pyramid":True, "bigtiff":False,
                      "strip":False, "compression":'VIPS_FOREIGN_TIFF_COMPRESSION_JPEG'}
    if (img.bands != 1 and img.bands != 3) or img.max() > 255:
        default_config["compression"] = 'VIPS_FOREIGN_TIFF_COMPRESSION_DEFLATE'

    config = {**default_config, **kwargs}
    img.tiffsave(str(filepath), **config)


class ImageSequential(object):
    """
    A sequential container that allows complex operations to multiple large image files.
    Input will be scaled to the first input.

    Example:
    >>> inputs = (filename1, filename2, filename3)
    >>> fn = lambda (x, y, z): y*x + (1-y)*z  # x could be a segmentation mask; y could be a mask of alpha values; z could be the original image.
    >>> seq = ImageSequential(inputs, fn, output="output.tiff")
    >>> seq.evaluate()

    Example 2:
    >>> inputs = (filename1, filename2, filename3)
    >>> fn = lambda *args: np.mean(args, axis=0)
    >>> seq = ImageSequential(inputs, fn, output="output.tiff")
    >>> seq.evaluate()
    """

    def __init__(self, inputs, fn=lambda x,y: x, output=None):

        self.define_inputs(inputs)
        self.define_operations(fn)
        self.output = output

    def define_operations(self, fn):
        self.fn = fn

    def define_inputs(self, inputs):
        self.inputs = inputs

    def evaluate(self, output_size=None, safe_mode=False, **kwargs):
        """
        :param output_size: tuple (width, height), 'max', or 'min'. If output size is None, outputs image according to the first input's size.
        :param safe_mode: For some reasons, VIPS seems to have some issues processing pyramid levels.
                          if safe_mode==True, converts pyvips image to array then back to vips object before saving.
        :param kwargs:
        :return:
        """
        imgs = []
        for img in self.inputs:
            if isinstance(img, pyvips.Image):
                imgs.append(img)
            elif isinstance(img, np.ndarray):
                imgs.append(arr2pyvips(img))
            else:
                imgs.append(pyvips.Image.tiffload(str(img)))

        wh = [(img.width, img.height) for img in imgs]
        if output_size is None:
            wh_ref = [wh[0][0], wh[0][1]]
            scalefactors = [(1.0, 1.0)]
        elif 'max' in output_size.lower():
            wh_ref = np.max(wh, axis=0)
            scalefactors = [wh_ref / np.array([wh[0][0], wh[0][1]]), ]
        elif 'min' in output_size.lower():
            wh_ref = np.min(wh, axis=0)
            scalefactors = [wh_ref / np.array([wh[0][0], wh[0][1]]), ]
        else:
            wh_ref = output_size
            scalefactors = [wh_ref / np.array([wh[0][0], wh[0][1]])]

        for wh_ in wh[1:]:
            scalefactors.append(np.array((wh_ref[0] / wh_[0],
                                          wh_ref[1] / wh_[1])))

        for i, img in enumerate(imgs):
            if np.max(scalefactors[i]) <= 0.5:
                try:
                    level = np.int(-np.log2(np.max(scalefactors[i])))
                    new_scalefactor = scalefactors[i] * np.power(2, level)
                    imgs[i] = pyvips_load(imgs[i].filename, level) # pyvips.Image.tiffload(str(self.inputs[i]), page=level)
                    scalefactors[i] = new_scalefactor   # Assigning new scalefactor here in case the previous line fails to load.
                except Exception as e:
                    print(e)
            imgs[i] = imgs[i].resize(scalefactors[i][0], vscale=scalefactors[i][1])

        img = self.fn(*imgs)

        if safe_mode:
            img = arr2pyvips(pyvips2arr(img))

        if self.output is not None:
            try:
                default_config = {"tile":True, "tile_width":512, "tile_height":512, "squash":False, "pyramid":True, "bigtiff":False,
                                  "strip":False, "compression":'VIPS_FOREIGN_TIFF_COMPRESSION_DEFLATE'}
                config = {**default_config, **kwargs}
                try:
                    img.tiffsave(str(self.output), **config)
                except:
                    config["bigtiff"] = True
                    img.tiffsave(str(self.output), **config)
                img = pyvips.Image.tiffload(str(self.output))   # Valid that the file is not corrupted.
            except Exception as e:
                print(e, file=sys.stderr)
                if Path(self.output).exists():
                    Path(self.output).unlink()
                raise(e)

        return img


class TileWorker(Process):

    def __init__(self, queue_in, queue_out, slidepath, tile_size, pad_w, pad_h, level=0, tissue_mask_fn=None, dtype=None):

        if (queue_in is not None) and (queue_out is not None):
            Process.__init__(self, name='TileWorker')
            self.daemon = True
        self._queue_in = queue_in
        self._queue_out = queue_out
        self._slidepath = slidepath
        self._tile_size = tile_size
        self.tile_size = tile_size
        self.pad_h = pad_h
        self.pad_w = pad_w
        self.tissue_mask_fn = tissue_mask_fn
        self.level = level
        self.dtype = dtype

    def run(self):

        while True:
            data_in = self._queue_in.get()
            if data_in is None:
                self._queue_out.put(None)
                break
            x, y = data_in

            tile, tile_mask = self.get_tile(x, y)

            if (tile_mask is None) or np.any(tile_mask):
                data_out = (tile, tile_mask, y + self.pad_h[0], x + self.pad_w[0])
                self._queue_out.put(data_out)
                # self._queue_in.task_done()

    def get_tile(self, x, y):

        if isinstance(self._slidepath, pyvips.Image):
            tile = pyvips_crop(self._slidepath, x, y, self.tile_size, self.tile_size)
        else:
            if not isinstance(self.level, int):
                try:
                    tiles = []
                    mid_x_slide = int(x + self.tile_size / 2)
                    mid_y_slide = int(y + self.tile_size / 2)
                    for level in self.level:
                        this_start_x = int(mid_x_slide * np.power(0.5, level - self.level[0]) - self.tile_size / 2)
                        this_start_y = int(mid_y_slide * np.power(0.5, level - self.level[0]) - self.tile_size / 2)
                        tile = pyvips_load(str(self._slidepath), level, this_start_x, this_start_y, self.tile_size, self.tile_size)
                        tiles.append(pyvips2arr(tile))
                    tile = np.stack(tiles)
                except:
                    tile = pyvips_load(str(self._slidepath), self.level, x, y, self.tile_size, self.tile_size)
                    tile = pyvips2arr(tile)
            else:
                tile = pyvips_load(str(self._slidepath), self.level, x, y, self.tile_size, self.tile_size)
                tile = pyvips2arr(tile)

        bands = tile.bands if isinstance(tile, pyvips.Image) else len(tile.shape)

        if (self.tissue_mask_fn is not None) and bands == 3:
            tile_mask = self.tissue_mask_fn(tile)
        elif (self.tissue_mask_fn is not None) and bands == 4:
            tile_mask = self.tissue_mask_fn(tile[0])
        else:
            tile_mask = None

        return tile, tile_mask


class TileProcessor(object):

    def __init__(self, tile_size, stride, output_transform=lambda output: output, tmp_dir=None, workers=8, debug=False, mask_fn=lambda x: x>0):
        """
        :param tile_size:
        :param stride: the size of stride is define by this
        :param model: the prediction function
        """
        self.tile_size = tile_size
        self.stride = stride
        self.output_transform = output_transform
        self.tmp_dir = tmp_dir
        self.WINDOW_SPLINE_2D = self._window_2D(self.tile_size, self.stride, power=2)
        self.workers = max(1, workers)
        self.debug = debug
        self.mask_fn = mask_fn

        if tmp_dir is not None:
            Path(tmp_dir).mkdir(exist_ok=True, parents=True)

    def _spline_window(self, window_size, stride, power=2):
        """
        Squared spline (power=2) window function:
        https://www.wolframalpha.com/input/?i=y%3Dx**2,+y%3D-(x-2)**2+%2B2,+y%3D(x-4)**2,+from+y+%3D+0+to+2
        """
        overlap = window_size - stride
        interp_wind_size = overlap * 2
        i = int(interp_wind_size / 4)
        wind_outer = (abs(2 * (signal.triang(interp_wind_size))) ** power) / 2
        wind_outer[i:-i] = 0
        wind_inner = 1 - (abs(2 * (signal.triang(interp_wind_size) - 1)) ** power) / 2
        wind_inner[:i] = 0
        wind_inner[-i:] = 0

        wind = wind_inner + wind_outer
        wind_l, wind_r = np.split(wind, 2)

        wind = np.concatenate([wind_l, np.ones(window_size - interp_wind_size), wind_r], axis=0)

        return wind

    def _window_2D(self, window_size, stride, power=2):
        """
        Make a 1D window function, then infer and return a 2D window function.
        Done with an augmentation, and self multiplication with its transpose.
        Could be generalized to more dimensions.
        """
        wind = self._spline_window(window_size, stride, power)
        wind = np.expand_dims(wind, 1)
        return wind * wind.transpose()

    def get_padding(self, width, height):
        pad_w = int(round(self.tile_size * (1 - 1.0 / (self.tile_size / self.stride)))) + \
                int((math.ceil(width / self.tile_size) - width / self.tile_size) * self.tile_size // 2)
        pad_w = (pad_w, pad_w + int((math.ceil(width / self.tile_size) - width / self.tile_size) * self.tile_size % 2))
        pad_h = int(round(self.tile_size * (1 - 1.0 / (self.tile_size / self.stride)))) + \
                int((math.ceil(height / self.tile_size) - height / self.tile_size) * self.tile_size // 2)
        pad_h = (pad_h, pad_h + int((math.ceil(height / self.tile_size) - height / self.tile_size) * self.tile_size % 2))
        return pad_w, pad_h

    def extract_tiles(self, slide, width, height, pad_w, pad_h, level=0):
        """
        :param img:
        :return: a generator
        """

        queue_in = Queue()
        queue_out = Queue()

        for _ in range(self.workers):
            TileWorker(queue_in, queue_out, slide, self.tile_size, pad_w, pad_h, level=level,
                       tissue_mask_fn=self.mask_fn).start()

        for y in np.arange(-pad_h[0], height, self.stride):
            for x in np.arange(-pad_w[0], width, self.stride):
                queue_in.put((x, y))
                # yield np.array(slide.read_region((x,y), 0, (self.tile_size, self.tile_size)))[:,:,:3], y+pad_h[0], x+pad_w[0]

        for _ in range(self.workers):
            queue_in.put(None)

        i = self.workers
        while i > 0:
            data = queue_out.get()
            if data is None:
                i -= 1
            else:
                yield data

    def extract_tiles2(self, slide, width, height, pad_w, pad_h, level=0):
        """
        Serial code for debugging
        :param img:
        :return: a generator
        """

        data = []
        tileworker = TileWorker(None, None, slide, self.tile_size, pad_w, pad_h, level=level, tissue_mask_fn=self.mask_fn)

        for y in np.arange(-pad_h[0], height, self.stride):
            for x in np.arange(-pad_w[0], width, self.stride):
                tile, tile_mask = tileworker.get_tile(x, y)
                data.append((tile, tile_mask, y + tileworker.pad_h[0], x + tileworker.pad_w[0]))

        return data

    def _merge_tiles(self, tiles, width, height, pad_w, pad_h, unpad):
        """
        :param tiles:
        :param padded_img_size:
        :return:
        """

        width += sum(pad_w)
        height += sum(pad_h)

        if not isinstance(tiles, list):
            tile_ = next(tiles)
        else:
            tile_ = tiles.pop(0)

        tile, y, x = tile_
        window_spline = self.WINDOW_SPLINE_2D
        tile_dtype = tile.dtype
        tile_dtype_numpy = tile.cpu().numpy().dtype

        if len(tile.shape) == 3:
            shape = (height, width, tile.shape[2])
            window_spline = np.expand_dims(window_spline, 2)
        else:
            shape = (height, width)

        # window_spline = np.broadcast_to(window_spline, tile.shape).astype(tile.dtype)
        window_spline = torch.tensor(np.broadcast_to(window_spline, tile.shape)).to(tile.device)

        tmp_file = SpooledTemporaryFile(dir=self.tmp_dir)
        output = np.memmap(tmp_file, dtype=np.float32, mode='w+', shape=shape)

        try:
            output[y:y + self.tile_size, x:x + self.tile_size] += (tile * window_spline).cpu().numpy()
        except ValueError:
            _shape = output[y:y + self.tile_size, x:x + self.tile_size].shape
            output[y:y + self.tile_size, x:x + self.tile_size] += (tile[:_shape[0], :_shape[1]] * window_spline[:_shape[0], :_shape[1]]).cpu().numpy()
        for tile, y, x in tiles:
            try:
                output[y:y + self.tile_size, x:x + self.tile_size] += (tile * window_spline).cpu().numpy()
            except ValueError:
                _shape = output[y:y + self.tile_size, x:x + self.tile_size].shape
                output[y:y + self.tile_size, x:x + self.tile_size] += (tile[:_shape[0], :_shape[1]] * window_spline[:_shape[0], :_shape[1]]).cpu().numpy()

        output = output.astype(tile_dtype_numpy)
        output = self.output_transform(output)

        return output[pad_h[0]:-pad_h[1], pad_w[0]:-pad_w[1]] if unpad else output

    def run(self, file, processing_fn, unpad=True, level=0):
        """

        :param filename:
        :param processing_fn:
        :param unpad:
        :param level:
        :return:
        """

        if isinstance(file, pyvips.Image):
            slide = file
        else:
            try:
                slide = pyvips_load(str(file), level=level)
            except:
                slide = pyvips_load(str(file), level=level[0])

        pad_w, pad_h = self.get_padding(slide.width, slide.height)
        width, height = slide.width, slide.height

        if self.debug:
            tiles = self.extract_tiles2(file, width, height, pad_w, pad_h, level=level)
            # kht.plot.multiplot(*[tile[0] for tile in tiles[5:10]])
            predictions = self._process2(processing_fn, tiles)  # [[torch.Tensor(y,x,ch), y, x], [...], [...]]
            # kht.plot.multiplot(*[pred[0].cpu().numpy().astype(np.float) for pred in predictions[5:10]])
            result = self._merge_tiles(predictions, width, height, pad_w, pad_h, unpad)
        else:
            tiles = self.extract_tiles(file, width, height, pad_w, pad_h, level=level)
            predictions = self._process(processing_fn, tiles)  # [[torch.Tensor(y,x,ch), y, x], [...], [...]]
            result = self._merge_tiles(predictions, width, height, pad_w, pad_h, unpad)

        return result

    def _process(self, processing_fn, tiles):
        for tile in tiles:
            res = processing_fn(*tile)
            if res is not None:
                yield res

    def _process2(self, processing_fn, tiles):
        res = []
        for tile in tiles:
            res.append(processing_fn(*tile))
        return res

    @staticmethod
    def write_to_tiff(array, filename, scale=None, **kwargs):
        # See https://jcupitt.github.io/libvips/API/current/VipsForeignSave.html#vips-tiffsave for tiffsave parameters
        dtype_to_format = {
            'uint8': 'uchar',
            'int8': 'char',
            'uint16': 'ushort',
            'int16': 'short',
            'uint32': 'uint',
            'int32': 'int',
            'float32': 'float',
            'float64': 'double',
            'complex64': 'complex',
            'complex128': 'dpcomplex',
        }

        height, width = array.shape[:2]
        bands = array.shape[2] if len(array.shape) == 3 else 1
        img = pyvips.Image.new_from_memory(array.ravel().data, width, height, bands, dtype_to_format[str(array.dtype)])
        if scale is not None:
            img = pyvips.Image.resize(img, scale)
        img.tiffsave(str(filename), **kwargs)
