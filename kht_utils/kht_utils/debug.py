import sys
import time
import inspect
import re
from functools import wraps
import gc
import torch
import warnings


def varname(p):
    """
    Returns the literal name of the variable
    :param p: variable
    :return: name of the variable as a string
    """
    for line in inspect.getframeinfo(inspect.currentframe().f_back)[3]:
        m = re.search(r'\bvarname\s*\(\s*([A-Za-z_][A-Za-z0-9_]*)\s*\)', line)
        if m:
            return m.group(1)


def debug_decorator(debug_fn=None, **kwargs):
    """
    Can be added as decorator to functions to debug their outputs.
    :param debug_fn:
    :param kwargs:
    :return:

    eg. 1: ------- this will save the mat file as 'test.mat'
    >>> @debug_decorator(savemat, filename='test.mat')
    >>> def random_image():
    >>>     return np.random.rand(5,5), np.random.rand(10,10)

    eg. 2: ------- this will plot the outputs with multiplot
    >>> @debug_decorator(multiplot, title=['title1', 'title2'])
    >>> def random_image():
    >>>     return np.random.rand(5,5), np.random.rand(10,10)

    """
    def fn_wrapper(fn):
        @wraps(fn)
        def wrapped_function():
            try:
                debug_fn(fn(), **kwargs) + 1
            except Exception as e:
                print('Error with "debug_wrapper in wrapping function "{}": '.format(fn.__name__), e)
            return fn
        return wrapped_function()
    return fn_wrapper


class TraceCalls(object):
    """
    Use as a decorator on functions that should be traced. Several
    functions can be decorated - they will all be indented according
    to their call depth.

    Sometimes, when executing algorithms with complex function call sequences, and especially ones that require recursion,
    it's useful to see what calls actually occurred during execution, their arguments and return values, and so on.
    https://eli.thegreenplace.net/2012/08/22/easy-tracing-of-nested-function-calls-in-python

    Example:

    >>> @TraceCalls()
    >>> def iseven(n):
    >>>     return True if n == 0 else isodd(n - 1)
    >>>
    >>> @TraceCalls()
    >>> def isodd(n):
    >>>     return False if n == 0 else iseven(n - 1)
    >>>
    >>> print(iseven(7))

    """
    def __init__(self, stream=sys.stdout, indent_step=2, show_ret=False):
        self.stream = stream
        self.indent_step = indent_step
        self.show_ret = show_ret

        # This is a class attribute since we want to share the indentation
        # level between different traced functions, in case they call
        # each other.
        TraceCalls.cur_indent = 0

    def __call__(self, fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            indent = ' ' * TraceCalls.cur_indent
            argstr = ', '.join(
                [repr(a) for a in args] +
                ["%s=%s" % (a, repr(b)) for a, b in kwargs.items()])
            self.stream.write('%s%s(%s)\n' % (indent, fn.__name__, argstr))

            TraceCalls.cur_indent += self.indent_step
            ret = fn(*args, **kwargs)
            TraceCalls.cur_indent -= self.indent_step

            if self.show_ret:
                self.stream.write('%s--> %s\n' % (indent, ret))
            return ret
        return wrapper


def isdebugging():
    """
    Check whether code is being run by the debugger
    :return:
    """
    for frame in inspect.stack():
        if frame[1].endswith("pydevd.py"):
            return True
    return False


def retryloop(attempts, timeout=None, delay=0, backoff=1):
    """
    retryloop Usage:

    Example:
        >>> for retry in retryloop(10, timeout=30):
        >>>    try:
        >>>        something
        >>>    except SomeException:
        >>>        retry()
        >>>
        >>> for retry in retryloop(10, timeout=30):
        >>>    something
        >>>    if somecondition:
        >>>        retry()

    Code Explanation: success={True} is passed out in the form of a generator 'attempt' times.
    If the last value returned by the generator is cleared then success is {True}
    """
    starttime = time.time()
    success = set()
    for i in range(attempts):
        success.add(True)
        yield success.clear
        if success:
            return
        duration = time.time() - starttime
        if timeout is not None and duration > timeout:
            break
        if delay:
            time.sleep(delay)
            delay = delay * backoff
    raise RetryError


class RetryError(Exception):
    pass


def get_methods(object, spacing=20): 
    """
    Finding what methods a Python object has
    https://stackoverflow.com/questions/34439/finding-what-methods-a-python-object-has
    :param object: 
    :param spacing: 
    :return: 
    """
    methodList = []
    for method_name in dir(object):
        try:
            if callable(getattr(object, method_name)):
                methodList.append(str(method_name))
        except:
            methodList.append(str(method_name))
    processFunc = (lambda s: ' '.join(s.split())) or (lambda s: s)
    for method in methodList:
        try:
            print(str(method.ljust(spacing)) + ' ' +
                  processFunc(str(getattr(object, method).__doc__)[0:90]))
        except:
            print(method.ljust(spacing) + ' ' + ' getattr() failed') 


## MEM utils - For memory profiling: https://gist.github.com/Stonesjtu ##
def mem_report():
    """Report the memory usage of the tensor.storage in pytorch
    Both on CPUs and GPUs are reported"""

    def _mem_report(tensors, mem_type):
        """Print the selected tensors of type

        There are two major storage types in our major concern:
            - GPU: tensors transferred to CUDA devices
            - CPU: tensors remaining on the system memory (usually unimportant)

        Args:
            - tensors: the tensors of specified type
            - mem_type: 'CPU' or 'GPU' in current implementation """
        print('Storage on %s' %(mem_type))
        print('-'*LEN)
        total_numel = 0
        total_mem = 0
        visited_data = []
        for tensor in tensors:
            if tensor.is_sparse:
                continue
            # a data_ptr indicates a memory block allocated
            data_ptr = tensor.storage().data_ptr()
            if data_ptr in visited_data:
                continue
            visited_data.append(data_ptr)

            numel = tensor.storage().size()
            total_numel += numel
            element_size = tensor.storage().element_size()
            mem = numel*element_size /1024/1024 # 32bit=4Byte, MByte
            total_mem += mem
            element_type = type(tensor).__name__
            size = tuple(tensor.size())

            print('%s\t\t%s\t\t%.2f' % (
                element_type,
                size,
                mem) )
        print('-'*LEN)
        print('Total Tensors: %d \tUsed Memory Space: %.2f MBytes' % (total_numel, total_mem) )
        print('-'*LEN)

    LEN = 65
    print('='*LEN)
    objects = gc.get_objects()
    print('%s\t%s\t\t\t%s' %('Element type', 'Size', 'Used MEM(MBytes)') )
    tensors = [obj for obj in objects if torch.is_tensor(obj)]
    cuda_tensors = [t for t in tensors if t.is_cuda]
    host_tensors = [t for t in tensors if not t.is_cuda]
    _mem_report(cuda_tensors, 'GPU')
    _mem_report(host_tensors, 'CPU')
    print('='*LEN)


def deprecated(reason):
    """
    This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.
    """

    if isinstance(reason, (type(b''), type(u''))):

        # The @deprecated is used with a 'reason'.
        #
        # .. code-block:: python
        #
        #    @deprecated("please, use another function")
        #    def old_function(x, y):
        #      pass

        def decorator(func1):

            if inspect.isclass(func1):
                fmt1 = "Call to deprecated class {name} ({reason})."
            else:
                fmt1 = "Call to deprecated function {name} ({reason})."

            @wraps(func1)
            def new_func1(*args, **kwargs):
                warnings.simplefilter('always', DeprecationWarning)
                warnings.warn(
                    fmt1.format(name=func1.__name__, reason=reason),
                    category=DeprecationWarning,
                    stacklevel=2
                )
                warnings.simplefilter('default', DeprecationWarning)
                return func1(*args, **kwargs)

            return new_func1

        return decorator

    elif inspect.isclass(reason) or inspect.isfunction(reason):

        # The @deprecated is used without any 'reason'.
        #
        # .. code-block:: python
        #
        #    @deprecated
        #    def old_function(x, y):
        #      pass

        func2 = reason

        if inspect.isclass(func2):
            fmt2 = "Call to deprecated class {name}."
        else:
            fmt2 = "Call to deprecated function {name}."

        @wraps(func2)
        def new_func2(*args, **kwargs):
            warnings.simplefilter('always', DeprecationWarning)
            warnings.warn(
                fmt2.format(name=func2.__name__),
                category=DeprecationWarning,
                stacklevel=2
            )
            warnings.simplefilter('default', DeprecationWarning)
            return func2(*args, **kwargs)

        return new_func2

    else:
        raise TypeError(repr(type(reason)))
