# Incomplete. For testing routines in kht_utils to make sure they are working properly.

import os, sys, time
from datetime import datetime
from pathlib import Path
import pytest

print(os.getcwd())
sys.path.insert(0, './')
import file_io


''' file_io '''
def test_clipboard():
    '''
    Testing clipboard_list() and clipboard_paste()
    '''
    x = [[1], [2, 3, 4], [5, 6, 7, 8]]
    file_io.clipboard_list(x)
    pasted = file_io.clipboard_paste()
    assert(pasted == ['1\t2\t5', '\t3\t6', '\t4\t7', '\t\t8'])


def test_json_io():
    '''
    Testing json_io()
    '''
    data = {'1':'test','2':3, 'foo':['b', 'a', 'r']}
    file_io.json_io('test.json', data)      # Saving data
    loaded = file_io.json_io('test.json')   # Loading data
    os.remove('test.json')
    assert(data==loaded)


def test_findbydate():
    path = Path.cwd()
    pattern = r'*temp*'

    delay = 0.5
    t = []

    for i in range(1,5):
        t.append(datetime.now())
        time.sleep(0.1)
        os.rmdir(path / f'temp{i}') if (path / f'temp{i}').exists() else None
        os.mkdir(path / f'temp{i}')
        time.sleep(delay)

    try:
        filepath1, date1 = file_io.findbydate(path, pattern, return_date=True)
        filepath2, date2 = file_io.findbydate(path, pattern, date=datetime.isoformat(t[1]), return_date=True)
        filepath3, date3 = file_io.findbydate(path, pattern, date=datetime.isoformat(t[2]), mode='after', return_date=True)
        filepath4, date4  = file_io.findbydate(path, pattern, date=datetime.isoformat(t[3]), mode='before', return_date=True)
    except Exception as e:
        os.rmdir(path / 'temp1')
        os.rmdir(path / 'temp2')
        os.rmdir(path / 'temp3')
        os.rmdir(path / 'temp4')
        raise e
    
    print("newest", date1, filepath1)
    print("closest", date2, filepath2)
    print("after", date3, filepath3)
    print("before", date4, filepath4)

    assert filepath1==path/'temp4'
    assert filepath2==path/'temp2'
    assert filepath3==path/'temp3'
    assert filepath4==path/'temp3'

    # Removing temp files
    os.rmdir(path / 'temp1')
    os.rmdir(path / 'temp2')
    os.rmdir(path / 'temp3')
    os.rmdir(path / 'temp4')
