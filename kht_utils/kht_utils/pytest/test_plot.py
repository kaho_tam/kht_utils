import os, sys, shutil
import pytest
import numpy as np
import matplotlib.pyplot as plt
import plot


def test_scatterthumb():
	# Generate data x, y for scatter and an array of images.
	x = np.arange(20)
	y = np.random.rand(len(x))
	arr = np.empty((len(x),30,30))
	for i in range(len(x)):
		f = np.random.rand(30,30)
		arr[i, :, :] = f

	test = plot.scatterthumb(x, y, arr, zoom=2, block=False)
	plt.close('all')


def test_multiplot():
	test = np.zeros((256,256))
	
	plot.multiplot(test, block=False)
	plot.multiplot(test, test, block=False)
	plot.multiplot(test, test, test, block=False)
	plt.close('all')


def test_plotly():
	# plotlyline
	N = 100
	random_x = np.linspace(0, 1, N)
	random_y0 = np.random.randn(N) + 5 + np.linspace(0, 10, N)
	random_y1 = np.random.randn(N) + np.linspace(0, 1, N)
	random_y2 = np.random.randn(N) - 5 + np.linspace(0, 10, N)

	N2 = 80
	random_x3 = np.linspace(0, 0.8, N2)
	random_y3 = np.random.randn(N2) - np.linspace(0, 10, N2)

	N2 = 80
	random_x4 = np.linspace(0.2, 1.0, N2)
	random_y4 = np.random.randn(N2) + np.linspace(0, 10, N2)

	plot.plotlyline(random_y1)
	plot.plotlyline([random_y0, random_y3], mode='lines')
	plot.plotlyline([[random_x3], [random_y3]], mode='lines')
	plot.plotlyline([[random_x], [random_y0, random_y1, random_y2]], layout={"title": "title", "xlabel": "foo", "ylabel": "bar"}, legend=["y1", "y2", "y3"], mode='lines')
	plot.plotlyline([[random_x, random_x3], [random_y0, random_y3]])

	# Plots with multiple y-axes
	plot.plotlyline([[random_x, random_x3], [random_y0, random_y3]], multi_ax=True, mode='lines')
	plot.plotlyline([[random_x], [random_y0, random_y1, random_y2]], layout={"title": "title", "xlabel": "foo", "ylabel": "bar"},
					legend=["y1", "y2", "y3"], mode='lines', multi_ax=True, auto_open=False)
	plot.plotlyline([[random_x, random_x, random_x, random_x3, random_x4], [random_y0, random_y1, random_y2, random_y3, random_y4]], multi_ax=True, auto_open=False, mode='lines')

	# plotlyheatmap
	z = [[1, 20, 30], [20, 1, 60], [30, 60, 1]]
	x = [1, 2, 3]
	y = ['a', 'b', 'c']
	plot.plotlyheatmap(z)
	plot.plotlyheatmap((z, x, y))
	plot.plotlyheatmap((z, x, y), layout={"title": "title", "xlabel": "foo", "ylabel": "bar"})

	os.remove('output.html')


def test_plotlylogger():

	logger = plot.PlotlyLogger()

	y1 = np.random.rand(100)
	y2 = np.random.rand(100)
	y3 = np.random.rand(50)

	for _y1, _y2 in zip(y1, y2):
		logger.update(["y1", "y2"], [_y1, _y2])

	logger.update("y3", y3)

	logger.saveplot()
	layout = {"title": "title", "xlabel": "Epoch", "ylabel": "Loss"}
	logger.saveplot(x=list(range(100, 200)), layout=layout)

	os.remove('output.html')
