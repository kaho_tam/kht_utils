import sys
import pytest
sys.path.insert(0, './')
import debug


def test_deprecated():
    @debug.deprecated('fn2 is deprecated')
    def fn2():
        print("This is fn2")

    with pytest.warns(DeprecationWarning):
        fn2()

