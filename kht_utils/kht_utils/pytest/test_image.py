import os, sys, shutil
from unittest import TestCase

import pytest
import numpy as np
import matplotlib.pyplot as plt
from skimage import data
import image


def test_mosaic():

    img1 = np.random.randint(0, 255, (256, 128)) / 255.
    img2 = np.random.randint(0, 255, (256, 128, 3)) / 255.
    img3 = np.random.rand(256, 128)
    img4 = data.astronaut() / 255.
    img5 = data.camera() / 255.
    imgs = [img1, img2, img3, img4, img5]

    for i in range(1, int(np.power(2,len(imgs)))):
        binary = ("{" + "0:0{}b".format(len(imgs)) + "}").format(i)
        _imgs = [x for j, x in enumerate(imgs) if (binary[j]=="1")]
        if len(_imgs) == 1:
            continue
        output = image.mosaic(*_imgs, nrows=0, ncols=0, min_perimeter=False, orientation=None)
        # print([x.shape for x in _imgs])
    pass

